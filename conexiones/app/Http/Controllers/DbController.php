<?php 
//  namespace App\Personalizado;
namespace App\Http\Controllers;
 use Illuminate\Support\Facades\DB;
 use Illuminate\Support\Facades\Storage;
 use Illuminate\Support\Facades\Config;

  
 class DbController extends Controller
 {
          
    public function conectar($id)
    {

        $res = app('db')->select("SELECT * FROM datos WHERE id = $id");

        if($res){
            $driv='mysql';
            $puerto_conex= 3306;
            $database_name= $res[0]->db_name;
            $user_name= $res[0]->user_name;
            $contra= $res[0]->pass;
            $host_con= $res[0]->host_name;

            Config::set([
                'database.connections.server_variable.driver'=>$driv,
                'database.connections.server_variable.database'=>$database_name,   
                'database.connections.server_variable.username'=>$user_name,
                'database.connections.server_variable.password'=>$contra,
                'database.connections.server_variable.port'=>$puerto_conex,
                'database.connections.server_variable.host'=>$host_con,
                
            ]);
            $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM datos");
            Config::set([
                'database.connections.server_variable' => null,
            ]);
            return $respuesta; 
        }else{
            return array('error' => 0, 'response'=> 'Id not found in db, please try another');
        }

    }

    public function conectar2($id)
    {
        $res = app('db')->select("SELECT * FROM datos WHERE id = $id");

        if($res){
            $driv='mysql';
            $puerto_conex= 3306;
            $database_name= $res[0]->db_name;
            $user_name= $res[0]->user_name;
            $contra= $res[0]->pass;
            $host_con= $res[0]->host_name;

            Config::set([
                'database.connections.server_variable.driver'=>$driv,
                'database.connections.server_variable.database'=>$database_name,   
                'database.connections.server_variable.username'=>$user_name,
                'database.connections.server_variable.password'=>$contra,
                'database.connections.server_variable.port'=>$puerto_conex,
                'database.connections.server_variable.host'=>$host_con,
                
            ]);
            $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM datos");
            Config::set([
                'database.connections.server_variable' => null,
            ]);
            return $respuesta; 
        }else{
            return array('error' => 0, 'response'=> 'Id not found in db, please try another');
        }
    }
}