<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use App\Helpers;

class WsController extends Controller
{
    public function consulta1(Request $request){
        if($request->method('POST')){
            if(verifyRequiredParams(array('plaza','cuenta'))){
                try{
                    $data = app('db')->select("SELECT * FROM plazas WHERE plaza = '$request->plaza' LIMIT 1;");
                    if($data){
                        $driv='mysql';
                        $puerto_conex= 3306;
                        $database_name= $data[0]->db_log;
                        $user_name= $data[0]->usr_log;
                        $contra= $data[0]->pass_log;
                        $host_con= $data[0]->host_log;

                        Config::set([
                            'database.connections.server_variable.driver'=>$driv,
                            'database.connections.server_variable.database'=>$database_name,   
                            'database.connections.server_variable.username'=>$user_name,
                            'database.connections.server_variable.password'=>$contra,
                            'database.connections.server_variable.port'=>$puerto_conex,
                            'database.connections.server_variable.host'=>$host_con,
                            
                        ]);

                        $respuesta = app('db')->connection('server_variable')->select("SELECT ndocto, fecdoc, vence, monto, sal_fac AS saldo_docto, d.ncta, d.nombre, d.dir_cte, d.sdo_act AS saldo_cte
                            FROM cxcgpa AS c
                            INNER JOIN dirctegp AS d
                            ON c.ncta = d.ncta
                            WHERE d.num_red > 0 AND c.ncta = $request->cuenta;");

                        return $respuesta;

                    }else{
                        return response()->json(["ERROR"=>1,"MSG"=>"ERROR AL REALIZAR CONSULTA"]);
                    }
                }catch (\Exception $e) {
                    return response()->json(["ERROR"=>1,"MSG"=>$e]);
                }
            }else{
               return response()->json(["ERROR"=>1,"MSG"=>"PARAMETROS INCORRECTOS"]);
            }
        }
    }

    public function consulta2(Request $request){
        if($request->method('POST')){
            if(verifyRequiredParams(array('plaza','cliente'))){
                try{
                    $data = app('db')->select("SELECT * FROM plazas WHERE plaza = '$request->plaza' LIMIT 1;");
                    if($data){
                        $driv='mysql';
                        $puerto_conex= 3306;
                        $database_name= $data[0]->db_fact;
                        $user_name= $data[0]->usr_fact;
                        $contra= $data[0]->pass_fact;
                        $host_con= $data[0]->host_fact;

                        Config::set([
                            'database.connections.server_variable.driver'=>$driv,
                            'database.connections.server_variable.database'=>$database_name,   
                            'database.connections.server_variable.username'=>$user_name,
                            'database.connections.server_variable.password'=>$contra,
                            'database.connections.server_variable.port'=>$puerto_conex,
                            'database.connections.server_variable.host'=>$host_con,
                            
                        ]);

                        $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM fgaspasa.facturas_xml WHERE num_cte = $request->cliente;");

                        return $respuesta;

                    }else{
                        return response()->json(["ERROR"=>1,"MSG"=>"ERROR AL REALIZAR CONSULTA"]);
                    }
                }catch (\Exception $e) {
                    return response()->json(["ERROR"=>1,"MSG"=>$e]);
                }
            }else{
               return response()->json(["ERROR"=>1,"MSG"=>"PARAMETROS INCORRECTOS"]);
            }
        }
    }

    public function consulta3(Request $request){
        if($request->method('POST')){
            if(verifyRequiredParams(array('plaza','cliente'))){
                try{
                    $data = app('db')->select("SELECT * FROM plazas WHERE plaza = '$request->plaza' LIMIT 1;");
                    if($data){
                        $driv='mysql';
                        $puerto_conex= 3306;
                        $database_name= $data[0]->db_red;
                        $user_name= $data[0]->usr_red;
                        $contra= $data[0]->pass_red;
                        $host_con= $data[0]->host_red;

                        Config::set([
                            'database.connections.server_variable.driver'=>$driv,
                            'database.connections.server_variable.database'=>$database_name,   
                            'database.connections.server_variable.username'=>$user_name,
                            'database.connections.server_variable.password'=>$contra,
                            'database.connections.server_variable.port'=>$puerto_conex,
                            'database.connections.server_variable.host'=>$host_con,
                            
                        ]);

                        $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM redes.captura_red WHERE num_cte = $request->cliente AND factura > 0;");

                        return $respuesta;

                    }else{
                        return response()->json(["ERROR"=>1,"MSG"=>"ERROR AL REALIZAR CONSULTA"]);
                    }
                }catch (\Exception $e) {
                    return response()->json(["ERROR"=>1,"MSG"=>$e]);
                }
            }else{
               return response()->json(["ERROR"=>1,"MSG"=>"PARAMETROS INCORRECTOS"]);
            }
        }
    }

    public function getHistorial(Request $request){
        if($request->method('POST')){
            if(verifyRequiredParams(array('plaza','cliente'))){
                try{
                    $data = app('db')->select("SELECT * FROM plazas WHERE plaza = '$request->plaza' LIMIT 1;");
                    if($data){
                        $driv='mysql';
                        $puerto_conex= 3306;
                        $database_name= $data[0]->db_red;
                        $user_name= $data[0]->usr_red;
                        $contra= $data[0]->pass_red;
                        $host_con= $data[0]->host_red;

                        Config::set([
                            'database.connections.server_variable.driver'=>$driv,
                            'database.connections.server_variable.database'=>$database_name,   
                            'database.connections.server_variable.username'=>$user_name,
                            'database.connections.server_variable.password'=>$contra,
                            'database.connections.server_variable.port'=>$puerto_conex,
                            'database.connections.server_variable.host'=>$host_con,
                            
                        ]);

                        $respuesta = app('db')->connection('server_variable')->select("SELECT lec_act,litros, importe,redes.fecha, sal_fac, factura, fecdoc,cxcgpa.VENCE vence, link_pdf, link_xml
                        FROM redes.captura_red redes
                        INNER JOIN fgaspasa.facturas_xml xml ON  xml.folio= redes.factura
                        INNER JOIN dblog.cxcgpa cxcgpa ON cxcgpa.ndocto=redes.factura
                        WHERE YEAR(redes.fecha) = YEAR(CURDATE()) AND redes.num_cte=$request->cliente
                        GROUP BY redes.factura
                        ORDER BY redes.fecha DESC;");

                        return $respuesta;

                    }else{
                        return response()->json(array("ERROR"=>1,"MSG"=>"ERROR AL REALIZAR CONSULTA"));
                    }
                }catch (\Exception $e) {
                    return response()->json(array("ERROR"=>1,"MSG"=>$e));
                }
            }else{
               return response()->json(array("ERROR"=>1,"MSG"=>"PARAMETROS INCORRECTOS"));
            }
        }
    }

    public function getAdeudoTotal(Request $request){
        if($request->method('POST')){
            if(verifyRequiredParams(array('plaza','cliente'))){
                try{
                    $data = app('db')->select("SELECT * FROM plazas WHERE plaza = '$request->plaza' LIMIT 1;");
                    if($data){
                        $driv='mysql';
                        $puerto_conex= 3306;
                        $database_name= $data[0]->db_log;
                        $user_name= $data[0]->usr_log;
                        $contra= $data[0]->pass_log;
                        $host_con= $data[0]->host_log;

                        Config::set([
                            'database.connections.server_variable.driver'=>$driv,
                            'database.connections.server_variable.database'=>$database_name,   
                            'database.connections.server_variable.username'=>$user_name,
                            'database.connections.server_variable.password'=>$contra,
                            'database.connections.server_variable.port'=>$puerto_conex,
                            'database.connections.server_variable.host'=>$host_con,
                            
                        ]);

                        $respuesta = app('db')->connection('server_variable')->select("SELECT SUM(sal_fac) total_pagar
                            FROM cxcgpa AS c
                            INNER JOIN dirctegp AS d
                            ON c.ncta = d.ncta
                            WHERE d.num_red > 0 AND c.ncta = $request->cliente;");

                        return $respuesta;

                    }else{
                        return response()->json(array("ERROR"=>1,"MSG"=>"ERROR AL REALIZAR CONSULTA"));
                    }
                }catch (\Exception $e) {
                    return response()->json(array("ERROR"=>1,"MSG"=>$e));
                }
            }else{
               return response()->json(array("ERROR"=>1,"MSG"=>"PARAMETROS INCORRECTOS"));
            }
        }
    }

}
