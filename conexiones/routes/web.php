<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/test/{id}', 'DbController@conectar');
// $router->get('/consulta1/{cnta}', 'WsController@consulta1');
// $router->get('/consulta2/{ncte}', 'WsController@consulta2');
// $router->get('/consulta3/{ncte}', 'WsController@consulta3');
$router->post('/consulta1', 'WsController@consulta1');
$router->post('/consulta2', 'WsController@consulta2');
$router->post('/consulta3', 'WsController@consulta3');
$router->post('/getHistorial', 'WsController@getHistorial');
$router->post('/getAdeudoTotal', 'WsController@getAdeudoTotal');

