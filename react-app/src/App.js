import logo from './logo.svg';
import './App.css';
import Header from './header/Header'

function App() {
  return (
    <div className="App">
      <Header/>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Welcome to my first react project 2021<br/>
          Almost
        </p>
      </header>
    </div>
  );
}

export default App;
