import './header.css'
import brandLogo from './logo192.png'

const Header = () => {
    return (
        <div className="header">
            <nav>
                <ul className="nav-brand">
                    <li>
                        <a 
                            className="App-link" 
                            href="/" rel="noopener noreferrer"
                        >
                            <img src={brandLogo}  alt="logo-brand"/>
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a 
                            className="App-link" 
                            href="/" rel="noopener noreferrer"
                        >
                        Home
                        </a>
                    </li>
                    <li>
                        <a 
                            className="App-link" 
                            href="/" rel="noopener noreferrer"
                        >
                        Nosotros
                        </a>
                    </li>
                    <li>
                        <a 
                            className="App-link" 
                            href="/" rel="noopener noreferrer"
                        >
                        Ayuda
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default Header;