// Declaración de variables
const nombre = 'golduck';
const pokemon_url = 'https://pokeapi.co/api/v2/pokemon/';

// DOM variables
const btnsend = document.getElementById('consultar');
const txtcomments = document.getElementById('comment');

// Declaración de funciones
const consultarAPI = () => {
  const data = getPokemons();
  // console.log('consultarAPI')
  console.log(data)
}

async function getPokemonsAsync(){
  const response = await fetch(pokemon_url + nombre)
  if(!response.ok){
    console.log('Failed!')
  } else {
    return response.json()
    // console.log(data)
  }
  return response;
}

function getPokemons(){
  const response = fetch(pokemon_url + nombre)
  .then(function(response){
    return response.json()
    // console.log('getPokemons')
  }).then(json => json.response)
  .catch(function(error){
    console.log(error)
  })
  return response;
}

// Declaración de listeners
btnsend.addEventListener('click', consultarAPI);


getPokemons();