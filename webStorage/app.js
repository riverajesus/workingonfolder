
const name1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    if(1 == 1)
      resolve({name: 'Jesus'})
    else
      reject(new Error('Error to obtain user info'))
  },1000)
})

const lastName = new Promise((resolve, reject) => {
  setTimeout(() => {
    if(1 == 1)
      resolve({lastName: 'Rivera'})
    else
      reject(new Error('Error to obtain user info'))
  },3000)
})

// name1.then(user => console.log(user)).catch(err => console.log(err))
// lastName.then(user => console.log(user)).catch(err => console.log(err))

Promise.all([name1,lastName]).then(result => console.log(result))