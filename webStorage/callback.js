console.log('Hello world')

function hola(callback){
  setTimeout(() => {
    callback('cuerpo')
  }, 1000)
}

function adios(callback){
  setTimeout(() => {
    callback('adiós')
  }, 1000)
}

hola((text) => {
  console.log(text)
  adios(t => console.log(t))
})