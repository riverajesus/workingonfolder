  // Buttons
  const btn_abrir = document.getElementById('btn-1')
  const btn_insertar = document.getElementById('btn-2')
  const btn_consultar = document.getElementById('btn-3')
  const btn_connection = document.getElementById('btn-4')

  // Textos
  const txt_estatus = document.getElementById('txt-estatus')
  const txt_database_name = document.getElementById('txt-database-name')
  const txt_version = document.getElementById('txt-version')
  const txt_useradd = document.getElementById('txt-useradd')

  let db // database conexion
  let cursor
  window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB
  window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {READ_WRITE: "readwrite"} // This line should only be needed if it is needed to support the object's constants for older browsers
  window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange

  if (!window.indexedDB) {
    console.error("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.")
  }else{
    console.log('Your browser supports a stable version of IndexedDB')
  }

  btn_abrir.addEventListener('click',openDbConexion)
  btn_insertar.addEventListener('click', addUser)
  btn_consultar.addEventListener('click', showRecords)
  btn_connection.addEventListener('click', checkConnection)

  function checkConnection(){
    if(window.navigator.onLine)
      console.log('conectado')
    else  
      console.log('sin conexión')
  }

  function openDbConexion(){
    const request = window.indexedDB.open(txt_database_name.value, txt_version.value)
    
    request.onerror = e => {
      // Do something with request.errorCode!
      console.log('failed conexion')
      return null
    }

    request.onupgradeneeded = e => {
      db = e.target.result
      try{
        const pNotes = db.createObjectStore('users',{keyPath: 'ssn'})
      }catch(ex){
        console.log('Exception: ',ex)  
      }

      console.log(`upgrade is called database name: ${db.name} and version: ${db.version}`)
    }

    request.onsuccess = e => {
      // Do something with request.result!
      db = e.target.result
      updateStatus()
    }
    
  } 

  function updateStatus(){
    if(db != null)
      txt_estatus.innerHTML = `Database: ${db.name} , Version: ${db.version}`
    console.log(db)
  }

  function addUser(){

    const userData = [
      { ssn: "444-44-4444", name: "Bill", age: 35, email: "bill@company.com" },
      { ssn: "555-55-5555", name: "Donna", age: 32, email: "donna@home.org" },
      { ssn: "555-55-5556", name: "Donna 2", age: 35, email: "donna@home1.org" },
      { ssn: "555-55-5557", name: "Rosita", age: 35, email: "rosita@home1.org" },
      { ssn: "555-55-5558", name: "Annel", age: 35, email: "annel@home1.org" }
    ]

    const tx = db.transaction('users', 'readwrite')
    const pUsers = tx.objectStore('users')

    pUsers.add(userData[txt_useradd.value])

    alert(userData[txt_useradd.value].name)
    updateStatus()  

  }

  function showRecords(){

    const pUsers = db.transaction('users','readonly').objectStore('users')

    cursor = null
    const request = pUsers.openCursor()
    request.onsuccess = e => {
      cursor = e.target.result
      if(cursor){
        txt_estatus.innerHTML += `-- ${cursor.value.name}`
        console.log(cursor)
        cursor.continue()
      }
        
    }

  }


  

  // console.log(db)

  // Consultar almacenamiento disponible en el navegador
  // const quota = navigator.storage.estimate()
  // console.log('quota: ', quota)