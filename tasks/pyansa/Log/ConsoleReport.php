<?php

namespace Pyansa\Log;

class ConsoleReport extends Report
{
    /**
     * Sobreescritura de Pyansa\Log\Report::fill
     *
     * @return void
     */
    protected function fill()
    {
        $this->id = strtoupper(uniqid());
        $this->exceptionClass = get_class($this->exception);
        $this->message = $this->exception->getMessage() ?: $this->exceptionClass;
        $this->file = $this->exception->getFile();
        $this->line = $this->exception->getLine();
        $this->memoryUsage = memory_get_usage(true);
        $this->memoryPeakUsage = memory_get_peak_usage(true);
        $this->phpProcessId = getmypid();
        $this->phpVersion = phpversion();
        $this->argv = $_SERVER['argv'];
        $this->argc = $_SERVER['argc'];
        $this->trace = $this->exception->getTrace();
    }
}
