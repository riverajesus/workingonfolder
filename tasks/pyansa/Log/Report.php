<?php

namespace Pyansa\Log;

use DateTime;
use Pyansa\Support\Fluent;
use Pyansa\Support\Debug\Dumper;

abstract class Report extends Fluent
{
    /**
     * Excepcion con la que se generara el report
     *
     * @var Exception
     */
    protected $exception;

    /**
     * Constructor de la clase
     *
     * @param  Exception
     * @return void
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
        $this->fill();
    }

    /**
     * Llena el reporte con informacion
     *
     * @return void
     */
    abstract protected function fill();

    /**
     * Magic method __toString
     *
     * @return string
     */
    public function __toString()
    {
        $str = "";
        foreach ($this->attributes as $key => $value) {
            $str .= sprintf("%s: %s", $key, Dumper::toString($value));
        }

        return $str;
    }
}
