<?php

namespace Pyansa\Log;

use Psr\Log\LoggerInterface;

class Logger implements LoggerInterface
{
    /**
     * El logger de Monolog que se implementa de forma interna
     *
     * @var Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Constructor de la clase
     *
     * @param  Psr\Log\LoggerInterface  $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Escribe un log.
     * Esta funcion sirve como punto comun de las otras funciones por nivel.
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    protected function writeLog($level, $message, $context)
    {
        $this->logger->{$level}($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::emergency
     * Registra un log de nivel emergency
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function emergency($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::alert
     * Registra un log de nivel alert
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function alert($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::critical
     * Registra un log de nivel critical
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function critical($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::error
     * Registra un log de nivel error
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function error($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::warning
     * Registra un log de nivel warning
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function warning($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::notice
     * Registra un log de nivel notice
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function notice($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::info
     * Registra un log de nivel info
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function info($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::debug
     * Registra un log de nivel debug
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function debug($message, array $context = [])
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::log
     * Registra un log del nivel proporcionado
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function log($level, $message, array $context = [])
    {
        $this->writeLog($level, $message, $context);
    }

    /**
     * Obtiene el logger interno
     *
     * @return Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Magic method __call
     * Redirige dinamicamente las llamadas de las funciones al logger interno.
     *
     * @param  string  $method
     * @param  array  $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        switch (count($args)) {
            case 0:
                return $this->logger->$method();
            case 1:
                return $this->logger->$method($args[0]);
            case 2:
                return $this->logger->$method($args[0], $args[1]);
            case 3:
                return $this->logger->$method($args[0], $args[1], $args[2]);
            case 4:
                return $this->logger->$method($args[0], $args[1], $args[2], $args[3]);
            default:
                return call_user_func_array(array($this->logger, $method), $args);
        }
    }
}
