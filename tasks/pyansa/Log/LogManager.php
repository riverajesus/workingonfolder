<?php

namespace Pyansa\Log;

use Monolog\Logger as Monolog;
use InvalidArgumentException;

class LogManager
{
    /**
     * Configuracion
     *
     * @var array
     */
    protected $config;

    /**
     * Channels resueltos
     *
     * @var array
     */
    protected $channels = [];

    /**
     * Constructor de la clase
     *
     * @param  array $config
     * @param  Illuminate\Contracts\Container\Container $app
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Intenta obtener un log channel
     *
     * @param  string  $name
     * @return Psr\Log\LoggerInterface
     */
    protected function get($name)
    {
        if (!isset($this->channels[$name])) {
            $this->channels[$name] = new Logger($this->resolve($name));
        }

        return $this->channels[$name];
    }

    /**
     * Resuelve el logger dado por $name
     *
     * @param  string  $name
     * @return Psr\Log\LoggerInterface
     * @throws InvalidArgumentException
     */
    protected function resolve($name)
    {
        $config = $this->config[$name];

        if (is_null($config)) {
            throw new InvalidArgumentException("Log channel [{$name}] is not defined.");
        }

        $driver = $config['driver'];
        if ($driver == 'daily') {
            return new DailyFileDriver($name, $config['path']);
        } else if ($driver == 'single') {
            return new SingleFileDriver($name, $config['path']);
        } else {
            throw new InvalidArgumentException("Driver [{$config['driver']}] is not supported.");
        }
    }

    /**
     * Obtiene un log channel
     *
     * @param  string|null  $channel
     * @return Psr\Log\LoggerInterface
     */
    public function channel($channel = null)
    {
        return $this->driver($channel);
    }

    /**
     * Obtiene un log driver
     *
     * @param  string|null  $driver
     * @return Psr\Log\LoggerInterface
     */
    public function driver($driver = null)
    {
        return $this->get($driver ?: $this->getDefaultDriver());
    }

    /**
     * Obtiene el driver default
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        if (isset($this->config['defaultChannel'])) {
            return $this->config['defaultChannel'];
        }

        return 'error';
    }

    /**
     * Obtiene los channels
     *
     * @return array
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::emergency
     * Registra un log de nivel emergency
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function emergency($message, array $context = [])
    {
        $this->driver()->emergency($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::alert
     * Registra un log de nivel alert
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function alert($message, array $context = [])
    {
        $this->driver()->alert($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::critical
     * Registra un log de nivel critical
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function critical($message, array $context = [])
    {
        $this->driver()->critical($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::error
     * Registra un log de nivel error
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function error($message, array $context = [])
    {
        $this->driver()->error($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::warning
     * Registra un log de nivel warning
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function warning($message, array $context = [])
    {
        $this->driver()->warning($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::notice
     * Registra un log de nivel notice
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function notice($message, array $context = [])
    {
        $this->driver()->notice($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::info
     * Registra un log de nivel info
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function info($message, array $context = [])
    {
        $this->driver()->info($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::debug
     * Registra un log de nivel debug
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function debug($message, array $context = [])
    {
        $this->driver()->debug($message, $context);
    }

    /**
     * Sobreescritura de Psr\Log\LoggerInterface::log
     * Registra un log del nivel proporcionado
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function log($level, $message, array $context = [])
    {
        $this->driver()->log($level, $message, $context);
    }
}
