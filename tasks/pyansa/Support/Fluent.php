<?php

namespace Pyansa\Support;

use ArrayAccess;
use JsonSerializable;

class Fluent implements ArrayAccess, JsonSerializable
{

    /**
     * Atributos de la instancia
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Constructor de la clase
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct($attributes = [])
    {
        foreach ($attributes as $key => $value)
        {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * Obtiene un atributo
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }

        return $default;
    }

    /**
     * Obtiene los atributos
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Convierte la instancia en un array
     *
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }

    /**
     * Sobreescritura de JsonSerializable::jsonSerialize
     * Convierte la instancia en algo serializable a JSON
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Sobreescritura de ArrayAccess::offsetExists
     * Determina si el offset existe
     *
     * @param  string  $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->{$offset});
    }

    /**
     * Sobreescritura de ArrayAccess::offsetGet
     * Obtiene el valor del offset proporcionado
     *
     * @param  string  $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->{$offset};
    }

    /**
     * Sobreescritura de ArrayAccess::offsetSet
     * Asigna el valor del offset proporcionado
     *
     * @param  string  $offset
     * @param  mixed   $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->{$offset} = $value;
    }

    /**
     * Sobreescritura de ArrayAccess::offsetUnset
     * Elimina el valor del offset proporcionado.
     *
     * @param  string  $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->{$offset});
    }

    /**
     * Magic method __get
     * Retorna dinamicamente el valor del atributo
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * Magic method __set
     * Asigna dinamicamente el valor del atributo
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * Magic method __isset
     * Verifica dinamicamente si un atributo esta establecido
     *
     * @param  string  $key
     * @return void
     */
    public function __isset($key)
    {
        return isset($this->attributes[$key]);
    }

    /**
     * Magic method __unset
     * Elimina dinamicamente un atributo
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->attributes[$key]);
    }

}
