<?php

namespace Pyansa\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade para el factory de Pyansa\Routing\ResponseFactory o en su defecto el response ya despachado
 */
class Response extends Facade
{
    /**
     * Sobreescritura de Illuminate\Support\Facades\Facade::getFacadeAccessor
     * Obtiene el nombre con el que se registro el componente
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'response.factory';
    }

    /**
     * Retorna un response JSON.
     * En caso que ya se haya ligado un response a la aplicacion, se usara ese.
     *
     * @param  string|array  $data
     * @param  integer $status
     * @param  array   $headers
     * @param  integer $options
     * @return Cake\Network\Response
     */
    public static function json($data = [], $status = 200, array $headers = [], $options = 0)
    {
        if (static::$app->bound('response')) {
            $response = static::$app['response'];
        } else {
            $response = static::make();
        }

        if (is_array($data)) {
            $data = json_encode($data, $options);
        }

        $response->body($data);
        $response->header($headers);
        $response->type('json');

        return $response;
    }
}
