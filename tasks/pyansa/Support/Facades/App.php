<?php

namespace Pyansa\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade para la aplicacion
 */
class App extends Facade
{
    /**
     * Sobreescritura de Illuminate\Support\Facades\Facade::getFacadeAccessor
     * Obtiene el nombre con el que se registro el componente
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'app';
    }
}
