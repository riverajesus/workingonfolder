<?php

namespace Pyansa\Support\Facades;

use Illuminate\Support\Facades\Facade;
use Cake\Network\Request as CakeRequest;

/**
 * Facade para el request de la aplicacion
 */
class Request extends Facade
{
    /**
     * Sobreescritura de Illuminate\Support\Facades\Facade::getFacadeAccessor
     * Obtiene el nombre con el que se registro el componente
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'request';
    }

    /**
     * Sobreescritura de Illuminate\Support\Facades\Facade::resolveFacadeInstance
     *
     * @param string $name
     * @return mixed
     */
    protected static function resolveFacadeInstance($name)
    {
        if (static::$app->bound($name)) {
            return parent::resolveFacadeInstance($name);
        }

        $instance = CakeRequest::createFromGlobals();
        static::$app->instance($name, $instance);

        return $instance;
    }
}
