<?php

namespace Pyansa\Exceptions;

use ErrorException;

/**
 * Esta clase se encarga de manejar los errores/excepciones que puedan presentarse en la aplicacion.
 */
abstract class Handler
{
    /**
     * Configuracion del handler
     *
     * @var array
     */
    protected $config;

    /**
     * Reporte (log) que contiene un id y template customizado a guardar
     *
     * @var Pyansa\Log\Report
     */
    protected $report;

    /**
     * Renderer de la excepcion
     *
     * @var Pyansa\Exceptions\RendererInterface
     */
    protected $renderer;

    /**
     * Indica si la aplicacion esta en modo debug
     *
     * @var boolean
     */
    public $debug;

    /**
     * Determina si se debe reportar la excepcion
     *
     * @var boolean
     */
    public $dontReport = false;

    /**
     * Determina si se debe renderizar la excepcion
     *
     * @var boolean
     */
    public $dontRender = false;

    /**
     * Determina si se debe terminar el script al manejar una excepcion
     *
     * @var boolean
     */
    public $dontExit = false;

    /**
     * Constructor de la clase
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $defaults = [
            'errorLevel' => -1,
            'formatMessage' => true,
            'errorReporting' => null,
            'displayErrors' => 'Off',
            'displayStartupErrors' => 'Off',
            'logErrors' => 'On',
            'errorLog' => null
        ];
        $this->config = $config + $defaults;
    }

    /**
     * Reporta una excepcion.
     *
     * @param  Exception  $exception
     * @return void
     */
    abstract public function report($exception);

    /**
     * Renderiza una excepcion
     *
     * @param  Exception  $exception
     * @return mixed
     */
    abstract public function render($exception);

    /**
     * Registra el handler para errores
     *
     * @return void
     */
    protected function registerErrorHandler()
    {
        set_error_handler([$this, 'handleError']);
    }

    /**
     * Registra el handler para excepciones
     *
     * @return void
     */
    protected function registerExceptionHandler()
    {
        set_exception_handler([$this, 'handleException']);
    }

    /**
     * Registra el handler para el shutdown
     *
     * @return void
     */
    protected function registerShutdownHandler()
    {
        register_shutdown_function([$this, 'handleShutdown']);
    }

    /**
     * Determina si el tipo de error es fatal.
     *
     * @param  integer $type
     * @return boolean
     */
    protected function isFatal($type)
    {
        return in_array($type, [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE]);
    }

    /**
     * Registra el handler en la aplicacion
     *
     * @return void
     */
    public function register()
    {
        $config = $this->config;

        // Nivel de errores a reportar
        if ($config['errorLevel'] !== null) {
            error_reporting($config['errorLevel']);
        }
        if ($config['errorReporting'] !== null) {
            error_reporting($config['errorReporting']);
        }

        // Mostrar errores
        if ($config['displayErrors'] !== null) {
            ini_set('display_errors', $config['displayErrors']);
        }

        // Mostrar errores iniciales de PHP
        if ($config['displayStartupErrors'] !== null) {
            ini_set('display_startup_errors', $config['displayStartupErrors']);
        }

        // Log nativo de errores de PHP
        if ($config['logErrors'] !== null) {
            ini_set('log_errors', $config['logErrors']);
        }

        // Archivo log nativo de PHP
        if ($config['errorLog'] !== null) {
            ini_set('error_log', $config['errorLog']);
        }

        $this->registerErrorHandler();
        $this->registerExceptionHandler();
        $this->registerShutdownHandler();
    }

    /**
     * Maneja un error
     *
     * @param  integer $level
     * @param  string  $message
     * @param  string  $file
     * @param  integer $line
     * @param  array   $context
     * @return void
     * @throws ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        if (error_reporting() & $level) {
            // Se llama la funcion handleException en vez de arrojar la excepcion ya que el exception handler
            // no cacha excepciones arrojadas desde aqui
            $this->handleException(new ErrorException($message, 0, $level, $file, $line));
        }
    }

    /**
     * Maneja una excepcion no capturada
     *
     * @param  Exception  $exception
     * @return void
     */
    public function handleUncaughtException($exception)
    {
        $this->handleException($exception);
    }

    /**
     * Maneja el evento shutdown
     *
     * @return void
     */
    public function handleShutdown()
    {
        $error = error_get_last();

        // If an error has occurred that has not been displayed, we will create a fatal
        // error exception instance and pass it into the regular exception handling
        // code so it can be displayed back out to the developer for information.
        if (!is_null($error) && $this->isFatal($error['type'])) {
            extract($error);
            $this->handleException(new FatalErrorException($message, 0, $type, $file, $line));
        }
    }

    /**
     * Maneja una excepcion.
     * Las funciones handlerError, handleUncaughtException y handleshutdown al final tendran que pasar a traves de esta
     * funcion.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function handleException($exception)
    {
        if (!$this->dontReport) {
            $this->report($exception);
        }
        if (!$this->dontRender) {
            $this->render($exception);
        }
        if (!$this->dontExit) {
            exit;
        }
    }
}
