<?php

namespace Pyansa\Exceptions\Formatters;

/**
 * Formatter encargado de formatear las excepciones del tipo PDOException tratando de omitir informacion sensible del sistema
 */
class PDOExceptionFormatter extends Formatter
{
    /**
     * Mensajes de error de acuerdo al codigo de error
     *
     * @var array
     */
    public static $messages = [
        "1005" => "Error de base de datos; No se ha podido crear una tabla.",
        "1006" => "Error de base de datos; No se ha podido crear una base de datos.",
        "1007" => "Error de base de datos; La base de datos que intenta crear ya existe.",
        "1008" => "Error de base de datos; La base de datos que intenta eliminar no existe.",
        "1009" => "Error de base de datos; No se ha podido eliminar una base de datos.",
        "1020" => "Error de base de datos; Un registro ha cambiado desde su lectura.",
        "1022" => "Error de base de datos; No se puede escribir, clave duplicada.",
        "1036" => "Error de base de datos; Una tabla es de solo lectura.",
        "1037" => "Error de base de datos; Memoria insuficiente.",
        "1040" => "Error de base de datos; Demasiadas conexiones.",
        "1041" => "Error de base de datos; Memoria insuficiente.",
        "1044" => "Error de base de datos; Acceso denegado a la base de datos.",
        "1045" => "Error de base de datos; Acceso denegado.",
        "1046" => "Error de base de datos; No se ha seleccionado una base de datos.",
        "1048" => "Error de base de datos; La consulta contiene columnas que no pueden ser null.",
        "1049" => "Error de base de datos; Base de datos desconocida.",
        "1050" => "Error de base de datos; La tabla que intenta crear ya existe.",
        "1051" => "Error de base de datos; Tabla desconocida.",
        "1052" => "Error de base de datos; La consulta contiene columnas ambiguas.",
        "1053" => "Error de base de datos; El servidor ha dejado de funcionar.",
        "1054" => "Error de base de datos; La consulta contiene columnas desconocidas.",
        "1058" => "Error de base de datos; La cantidad de columnas no corresponde con la cantidad de valores.",
        "1059" => "Error de base de datos; Identificador demasiado largo.",
        "1060" => "Error de base de datos; El nombre de la columna esta duplicado.",
        "1061" => "Error de base de datos; El nombre de la clave esta duplicado.",
        "1062" => "Error de base de datos; Informacion duplicada para una clave.",
        "1064" => "Error de base de datos; Error de parseo en la consulta.",
        "1105" => "Error de base de datos; Error desconocido.",
        "1106" => "Error de base de datos; Procedimiento desconocido.",
        "1107" => "Error de base de datos; Cantidad de parametros incorrecta para el procedimiento.",
        "1108" => "Error de base de datos; Parametros incorrectos para el procedimiento.",
        "1109" => "Error de base de datos; Tabla desconocida.",
        "1114" => "Error de base de datos; La tabla esta llena.",
        "1115" => "Error de base de datos; Charset desconocido.",
        "1116" => "Error de base de datos; Demasiadas tablas en un JOIN.",
        "1117" => "Error de base de datos; Demasiadas columnas.",
        "1122" => "Error de base de datos; No se puede cargar la funcion definida por el usuario.",
        "1123" => "Error de base de datos; No se puede inicializar la funcion definida por el usuario.",
        "1128" => "Error de base de datos; Funcion indefinida.",
        "1146" => "Error de base de datos; La tabla no existe.",
        "1149" => "Error de base de datos; Error de sintaxis en la consulta.",
        "1152" => "Error de base de datos; Conexion abortada.",
        "1153" => "Error de base de datos; El paquete de datos es demasiado grande.",
        "1166" => "Error de base de datos; Nombre de columna incorrecto.",
        "1169" => "Error de base de datos; No se puede escribir debido a una restricción de clave unica.",
        "1172" => "Error de base de datos; El resultado consiste en mas de un registro.",
        "1193" => "Error de base de datos; Variable de sistema desconocida.",
        "1194" => "Error de base de datos; La tabla esta corrupta y debe repararse.",
        "1216" => "Error de base de datos; No se puede agregar o actualizar un registro; no existe el registro con el cual se quiere relacionar.",
        "1217" => "Error de base de datos; No se puede eliminar o actualizar el registro; existen otros registros relacionados a este.",
        "1242" => "Error de base de datos; La subconsulta retorna mas de un registro.",
        "1280" => "Error de base de datos; El nombre del indice es incorrecto.",
        "1305" => "Error de base de datos; El procedimiento almacenado no existe.",
        "1317" => "Error de base de datos; La consulta ha sido interrumpida",
        "1365" => "Error de base de datos; Division entre cero",
        "1451" => "Error de base de datos; No se puede eliminar o actualizar un registro; existen otros registros relacionados a este.",
        "1452" => "Error de base de datos; No se puede agregar o actualizar el registro; no existe el registro con el cual se quiere relacionar."
    ];

    /**
     * Sobreescritura de Pyansa\Exceptions\Formatters\Formatter::format
     * Formatea el mensaje de la excepcion de acuerdo al codigo de error.
     *
     * @return string|null
     */
    protected function format($exception)
    {
        $errorCode = $exception->errorInfo[1];
        if (isset(static::$messages[$errorCode])) {
            return static::$messages[$errorCode];
        }

        return null;
    }
}
