<?php

namespace Pyansa\Exceptions\Formatters;

/**
 * Formatter encargado de formatear las excepciones del tipo ErrorException tratando de omitir informacion sensible del sistema
 */
class ErrorExceptionFormatter extends Formatter
{
    /**
     * Mapea la gravedad del error a un texto
     *
     * @param integer $severity
     * @return string
     */
    protected function mapErrorSeverity($severity)
    {
        switch ($severity) {
            case E_STRICT:
                $text = "Error de rigurosidad";
                break;
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $text = "Error de obsolescencia";
                break;
            case E_PARSE:
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case E_USER_ERROR:
                $text = "Error fatal";
                break;
            case E_WARNING:
            case E_USER_WARNING:
            case E_COMPILE_WARNING:
            case E_RECOVERABLE_ERROR:
                $text = "Error de advertencia";
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
                $text = "Error de aviso";
                break;
            default:
                $text = "Error desconocido";
                break;
        }

        return $text;
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\Formatters\Formatter::format
     * Formatea el mensaje de la excepcion.
     *
     * @return string
     */
    protected function format($exception)
    {
        $severity = $this->mapErrorSeverity($exception->getSeverity());
        $message = $exception->getMessage();

        return $severity . '. ' . $message;
    }
}
