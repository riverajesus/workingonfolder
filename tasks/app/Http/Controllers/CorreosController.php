<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailStatus;
use Illuminate\Support\Facades\Log;

class CorreosController extends Controller
{
    public function checkStatus(){
        $asunto = "Estatus de correos enviados schedule";
        try{
            $res = app('db')->select("SELECT CONCAT( SUM((SELECT COUNT(*) FROM caligas WHERE enviado = 1 ) + (SELECT COUNT(*) FROM gaspasa  WHERE enviado = 1 ) + (SELECT COUNT(*) FROM diesgas WHERE enviado = 1 )), '') total_enviados, (SELECT COUNT(*) FROM gaspasa WHERE enviado = 1) gaspasa, (SELECT COUNT(*) FROM diesgas WHERE enviado = 1) diesgas, (SELECT COUNT(*) FROM caligas WHERE enviado = 1) caligas;");
            $mensaje = array("status" => 1, "total_enviados" => $res[0]->total_enviados,"gaspasa" => $res[0]->gaspasa, "diesgas" => $res[0]->diesgas, "caligas" => $res[0]->caligas);
            Mail::to(["2016030154@upsin.edu.mx"])->send(new MailStatus($mensaje,$asunto));
        }catch(\Exception $ex){
            $mensaje = array("status" => 0, "res" => 'Error al enviar mensaje');
            Mail::to("2016030154@upsin.edu.mx")->send(new MailStatus($mensaje,$asunto));
            return array("status" => 0, "res" => $ex);
        }
        return array("status" => 1, "res" => $mensaje);
    }
}
