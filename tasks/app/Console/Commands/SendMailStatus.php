<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Support\DripEmailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use App\Mail\MailStatus;

class SendMailStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendMailStatus';

    /**
     * Conexion a la base de datos
     *
     * @var Illuminate\Database\Connection
     */
    public $db;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a marketing email to a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \App\Support\DripEmailer  $drip
     * @return mixed
     */
    public function handle()
    {
      $asunto = "Estatus de correos enviados schedule";
      try{
          $res = app('db')->select("SELECT CONCAT( SUM((SELECT COUNT(*) FROM caligas WHERE enviado = 1 ) + (SELECT COUNT(*) FROM gaspasa  WHERE enviado = 1 ) + (SELECT COUNT(*) FROM diesgas WHERE enviado = 1 )), '') total_enviados, (SELECT COUNT(*) FROM gaspasa WHERE enviado = 1) gaspasa, (SELECT COUNT(*) FROM diesgas WHERE enviado = 1) diesgas, (SELECT COUNT(*) FROM caligas WHERE enviado = 1) caligas;");
          $mensaje = array("status" => 1, "total_enviados" => $res[0]->total_enviados,"gaspasa" => $res[0]->gaspasa, "diesgas" => $res[0]->diesgas, "caligas" => $res[0]->caligas);
          Mail::to(["2016030154@upsin.edu.mx"])->send(new MailStatus($mensaje,$asunto));
      }catch(\Exception $ex){
          $mensaje = array("status" => 0, "res" => 'Error al enviar mensaje');
          Mail::to("2016030154@upsin.edu.mx")->send(new MailStatus($mensaje,$asunto));
          return array("status" => 0, "res" => $ex);
      }
      // return array("status" => 1, "res" => $mensaje);
    }
}