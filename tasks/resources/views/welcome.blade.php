<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>My first task</title>
</head>
<body>
  <h2>Welcome</h2>
  <p>this is my first practice about schedule tasks in lumen</p>
  @if(isset($msg['total_enviados']))
    <p>Se han enviado {{ $msg['total_enviados'] }} correos.</p>
    <ul>
      <li>Gaspasa: {{ $msg['gaspasa'] }}</li>
      <li>Diesgas: {{ $msg['diesgas'] }}</li>
      <li>Caligas: {{ $msg['caligas'] }}</li>
    </ul>
  @endif
</body>
</html>