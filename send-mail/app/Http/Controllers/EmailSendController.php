<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSend;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class EmailSendController extends Controller
{
    public function index(){
        $asunto = "Mail cartas llenafacil";
        try{
            Mail::to("2016030154@upsin.edu.mx")->send(new EmailSend($asunto));
            dd('Correo enviado');
        }catch(Exception $ex){
            dd($ex);
        }
    }
    public function viewsend(){
        return view('send');
    }

    public function download(){
        // $pathToFile = "D:\descargar\info.txt";
        
        // $filename = 'info.txt';
        // $tmpFile = tempnam(sys_get_temp_dir(), $filename);
        // copy($pathToFile, $tmpFile);

        // return response()->download($tmpFile, $filename);
        return Storage::download(public_path('descargar/info.txt'));
    }
}
