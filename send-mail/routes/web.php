<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmailSendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('send-mail', [EmailSendController::class, 'index']);
Route::get('send', [EmailSendController::class, 'viewsend']);


Route::get('/download', [EmailSendController::class, 'download']);
