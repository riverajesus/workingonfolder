<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Grid</title>
</head>
<style>
    *{
        margin: 0;
        padding: 0;
    }
    body{
        font-family: sans-serif;;
    }
    body p{
        font-size: calc(1.5vh + (.3*1vw - .1*1vw));
    }
    body td{
        font-size: calc(1.5vh + (.3*1vw - .1*1vw));
    }
    .container{
        position: relative;
        width: 100%;
        max-width: 800px;
        margin: 0 auto;

        display: flex;
        flex-wrap: wrap;
        justify-content: center;

        text-align: justify;
    }
    .item-2{
        width: 50%;
        min-width: 399px;
        height: auto;
        margin: 1em 0 1em 0;
    }
    .item-1{
        width: 100%;
        margin: 1em 0 1em 0;
        padding: 0 1.5em 0 1.5em;
    }
    .item-1-1{
        width: 52%;
        min-width: 399px;
        height: auto;
        margin: 1em 0 1em 0;
    }
    .text-center{
        text-align: center;
    }



    .bold{
        font-weight: 600;
    }
    .table-cargos {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        /* font-size: 16px; */
    }

    .table-cargos td, .table-cargos th {
        border: 1px solid rgba(255, 159, 64, 1);
        padding: 2px;
    }

    /* .table-cargos tr:nth-child(even){background-color: #f2f2f2;} */

    /* .table-cargos tr:hover {background-color: #ddd;} */

    .table-cargos th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        /* background-color: #EC880E; */
        background-color: rgba(255, 159, 64, 0.2);
        color: rgba(213, 120, 28, 0.9);
        font-weight: bolder;

    }

    .table-other {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        /* font-size: 16px; */
    }

    .table-other td:first-child {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: left;
        background-color: rgba(255, 159, 64, 0.2);
        color: rgba(213, 120, 28, 0.9);
        font-weight: bolder;
    }

    .table-other td, .table-other th {
        /* border: 1px solid #ddd;
        padding: 2px; */
        /* background-color: rgba(255, 159, 64, 0.2);
        color: rgba(213, 120, 28, 0.9); */
        border: 1px solid rgba(255, 159, 64, 1);
        padding: 2px;
    }



    .card{
        margin-left: 15px;
        border: 1px solid rgba(255, 159, 64, 1);
    }

    .card p{
        /* font-size: 17px; */
        padding: 0px 15px 0px 15px;
        margin: 0px;
    }
    .cochito{
        display: flex;
        justify-content: center;
    }

    .section-links{
        display: flex;
        justify-content: center;
    }
    .links{
        position: relative;
        display: flex;

        justify-content: space-around;
    }
    .socialLinks{
        display: block;
        margin: 0 10px 0 10px;
        text-decoration: none;
    }
    .socialLinks img{
        max-width: 60px;
        width: 100%;
        cursor: pointer;
    }
</style>
<body>
    <div class="container">
        <div class="item-1">
            <a class="links" href="{{ url('/') }}">
                <img src="{{asset('img/logo-lineas_1.svg')}}"/>
            </a>
            <p class="bold"> 
                Estimado(a): MARCIA JIMENEZ MORENO ROMERO
            </p>
            <p>
                Agradecemos su compra hecha el día 02 de agosto de 2021, por un importe de $1561.20. Le fueron surtidos
                117.60 litros de gas tratado con nuestro exclusivo aditivo, el cual mejora sustancialmente la combustión del
                gas brindandole considerables ahorros para beneficio de su economía.
            </p>
        </div>
        <div class="item-2">
            <img src="{{asset('img/tanque.svg')}}" alt="">
        </div>
        <div class="item-2">
            <img src="{{asset('img/tanque.svg')}}" alt="">
        </div>
        <div class="item-1-1">
            <img src="{{asset('img/tanque.svg')}}" alt="">
        </div>
        <div class="item-2">
            <p>
                De acuerdo a nuestro sistema de pagos, esta compra será cargada a su TARJETA DE CREDITO SANTANDER MEXICANO en 2 pagos, en las siguientes fechas: 
            </p>
        </div>
        <div class="item-2">
            <img src="assets/img/tanque.svg" alt="">
        </div>
        <div class="item-2">
            <p style="text-align: center; padding: 0; line-height: 1rem;">SUMA PUNTOS</p><br>
            <table class="table-cargos">
                <thead>
                    <tr>
                        <th>Cargo</th>
                        <th>Fecha</th>
                        <th>Importe</th>
                        <th>Uso sistema</th>
                        <th>Total</th>
                    </tr>                                                    
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>29-10-2021</td>
                        <td>50.25</td>
                        <td>11.54</td>
                        <td>61.79</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>29-10-2021</td>
                        <td>50.25</td>
                        <td>11.54</td>
                        <td>61.79</td>
                    </tr>
                </tbody>
            </table>  
            <br>
            <p style="text-align: center; padding: 0; line-height: 1rem;">ESTADO DE CUENTA</p> <br>  
            <table class="table-other">
                <tbody>
                    <tr>
                        <td>Anteriores:</td>
                        <td>61.79</td>
                    </tr>
                    <tr>
                        <td>Se acumularan:</td>
                        <td>61.79</td>
                    </tr>
                    <tr>
                        <td>Saldo nuevo:</td>
                        <td>61.79</td>
                    </tr>
                </tbody>
            </table>     
        </div>
        <div class="item-2">
            <div class="card">
                <p class="bold text-center">Puntos acumulados</p>
                <p>
                    Por esta compra usted acumulará 0 puntos más,
                    una vez que hayan sido realizados todos los
                    pagos oportunamente. Recuerde que al acumular
                    50 puntos los podrá canjear por 50 litros de 
                    ¡Gas Gratis!
                </p>
                <br>
                <div class="cochito">
                    <img src="{{asset('img/cochito.svg')}}" alt="" width="200px">
                </div>
            </div>
        </div>
        <div class="item-1">
            <p class="msj-inicio">                                                                                                        
                De acuerdo a su consumo promedio diario de Gas, le surtiremos nuevamente el Jueves 25 de Marzo de 2021.
                Para saber más acerca de nuestro sistema SUMA PUNTOS, consulte nuestro siti web o síganos en Facebook. <br>
                Muchas gracias por su preferencia.
            </p>
        </div>
        <div class="item-1">
            <div class="section-links">
                <div class="links">
                    <a class="socialLinks" href="https://www.facebook.com/GaspasaContigo" target="_blank">
                        <img class="socialIcon" src="{{asset('img/fb.png')}}" width="60" />
                    </a>
                    <a class="socialLinks" href="https://www.instagram.com/gaspasa_contigo_siempre/" target="_blank">
                        <img class="socialIcon" src="{{asset('img/ig.png')}}" width="60"/>
                    </a>
                    <a class="socialLinks" href="https://www.gaspasa.com.mx/" target="_blank"> 
                        <img class="socialIcon" src="{{asset('img/web.png')}}" width="60"/>
                    </a>
                    <a class="socialLinks" href="https://wa.me/526691010101" target="_blank"> 
                        <img class="socialIcon" src="{{asset('img/whatsapp.png')}}" width="60" />
                    </a>
                </div>                
            </div>            
        </div>
    </div>    
</body>
</html>
