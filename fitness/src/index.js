// const element = document.createElement("h1")
// element.innerText = 'Hello world'
// const container = document.getElementById("root")
// container.appendChild(element)

import React from "react"
import ReactDom from 'react-dom'

const users = [
  {
    name: 'Jesus',
    lastName: 'Rivera',
    avatar: 'https://cdn-icons-png.flaticon.com/512/4478/4478094.png'
  },
  {
    name: 'Annel',
    lastName: 'Rivera',
    avatar: 'https://cdn-icons-png.flaticon.com/512/4478/4478100.png'
  }
]

function getFullName(user){
  return `${user.name} ${user.lastName}`;
}

function getGreeting(){
  if(users[0]){
    return `Hello ${getFullName(users[0])}`
  }
  return `Hello stranger`
}

const element = (
  <div>
    <h1>{getGreeting()}</h1>
    <img src="{users[1].avatar}" alt="" />
  </div>
);
const container = document.getElementById('root')

ReactDom.render(element, container);
