<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CorreoClientes extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "¡Felices fiestas!";
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensaje, $subject)
    {          
        $this->msg = $mensaje;
        $this->subject = $subject;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('clientes.clientes'); 
    }
}