<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\CorreoClientes;

class TestController extends Controller
{
    public function index($id,$lote){
        set_time_limit(0);
        $empresa = '';

        if($id == 1){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/gaspasa/_MAIL.jpg",
                'id' => $id
            );
            $res = app('db')->select("SELECT * FROM gaspasa WHERE enviado = 0");
            $empresa = "gaspasa";
        }
        if($id == 2){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/diesgas/_MAIL.jpg",
                'id' => $id
            );
            $res = app('db')->select("SELECT * FROM diesgas WHERE enviado = 0");
            $empresa = "diesgas";
        }
        if($id == 3){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/caligas/_MAIL.jpg",
                'id' => $id
            );
            $res = app('db')->select("SELECT * FROM caligas WHERE enviado = 0;");
            $empresa = "caligas";
        }

        $count = count($res);
        $asunto = "¡Felices Fiestas!";
        $contador = 1;
        // dd($res);
        for($x=0;$x<$count;$x++)
        {
            //Mailecho $x;
            try {
                Mail::to("grivera@sersi.com.mx")->send(new CorreoClientes($mensaje,$asunto));
                $respuesta = app('db')->select("UPDATE $empresa SET enviado = 1 WHERE correo ='".$res[$x]->correo."';");
                return json_encode(array('error' => 0,'estatus' => 'not yet', 'counter' => $contador, 'response'=> 'Correos enviado con éxito!'));
            } catch (\Exception $e) {
                return json_encode(array('error' => 1,'estatus' => 'not yet', 'counter' => $contador, 'response'=> $e));
            }
            sleep(2);
            if($contador == $lote && $x < $count)
            {
               //sleep(2);
                // $contador = 0;
                // echo "lote hasta ".$x;
                return json_encode(array('error' => 0,'estatus' => 'not yet', 'counter' => $contador, 'response'=> 'Correos enviado con éxito!'));
            }
            $contador++;
        }

        return json_encode(array('error' => 0, 'estatus' => 'finished', 'counter'=> $contador, 'response'=> 'Correos enviado con éxito!'));
    }
    public function servicio(){
        $res = app('db')->select("SELECT * FROM gaspasa WHERE enviado = 0;");
        $data = count($res);
        return view('servicio', compact('data'));
    }

}
