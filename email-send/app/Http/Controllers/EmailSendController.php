<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSend;
use Illuminate\Support\Str;

class EmailSendController extends Controller
{
    public function index(){
        $asunto = "Mail cartas llenafacil";
        $sendTo = array(
            // "grivera@sersi.com.mx",
            "2016030154@upsin.edu.mx",
            // "jesusguerrero97@hotmail.com"
        );
        Mail::to($sendTo)->send(new EmailSend($asunto));

        dd(array("estatus" => "200", "sendedTo" => $sendTo ));
    }
}
