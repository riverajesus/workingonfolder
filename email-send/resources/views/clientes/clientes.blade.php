<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Correo</title> 
        <style>
            .links{
               text-decoration: none;
            }
            #mainTable{
                width: 100%;
                border-top-width: 0px;
                border-right-width: 0px;
                border-bottom-width: 0px;
                border-left-width: 0px;
                -webkit-border-horizontal-spacing: 0px;
                -webkit-border-vertical-spacing: 0px;
                display: table;
                border-collapse: separate;
                box-sizing: border-box;
                text-indent: initial;
                border-spacing: 2px;
                border-color: grey;
                line-height: normal !important;

            }
            
            .logo{
                width: 260px;
                height: 150px;
                margin: 0; 
                padding: 0; 
                outline: none; 
                text-decoration: none; 
                -ms-interpolation-mode: bicubic; 
                border: none; 
                display: block;
            }
            
            
             body{
                margin: 0;
                padding: 0;
                background: #ecf2f7;
                width: 100% !important;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                line-height: normal !important;
             }
             .tdTable{
                border-collapse: collapse; 
                border-spacing: 0; 
                margin: 0; 
                padding: 0;
            }
            .table-2
            {
                border-collapse: collapse; 
                border-spacing: 0;
                padding: 0;
                background: white;
                border-top-width: 0px;
                border-right-width: 0px;
                border-bottom-width: 0px;
                border-left-width: 0px;
                margin-inline-start: auto;
                margin-inline-end: auto;
                -webkit-border-horizontal-spacing: 0px;
                -webkit-border-vertical-spacing: 0px;
            }
            .tr-span{
                background: #B9C2CA;
                font-family: sans-serif;
            }
            .td-span{
                vertical-align:middle;
            }
            tbody{
                display: table-row-group;
                vertical-align: middle;
                border-color: inherit;
            }
        </style>
    </head>
    <body  style="overflow:hidden">
        
        <table id="mainTable">
            <tr>
                <td align="center" valign="top" class="tdTable">
                    <table border="0"  cellpadding="0" cellspacing="0" style="width: 646px;" class="table-2">
                        <tr style="height:45px;" class="tr-span">
                            <td align="center" align="center" valign="top" style="font-family:Arial, sans-serif, Helvetica;" class="td-span">
                                    <span style="font-size:11px;">Si no puedes ver las imágenes de este correo da <a target="_blank" href="{{url('/clientesIndex/'.$msg['id'])}}">clic aquí</a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" class="tdTable">
                                <img src="{{url($msg['imagen'])}}" width="646"/>
                            </td>
                        </tr>
                        <tr style="height:45px;" class="tr-span" >
                            <td align="center" style="font-family:Arial, sans-serif, Helvetica;" class="td-span">
                                    <span style="font-size:11px;">
                                        Agrega <a target="_blank" href="mailto:ayuda@sersi.com.mx">ayuda@sersi.com.mx</a>  a tus contactos
                                    </span>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
    
        
    </body>
</html>