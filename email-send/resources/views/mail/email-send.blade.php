<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{-- <link rel="stylesheet" href="assets/css/styles-grid-all.css"> --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <title>Grid</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        html body{
            font-family: sans-serif;
            color: #000;
        }
        .container{
            position: relative;
            width: 80%;
            max-width: 750px;
            margin: 0 auto;
            min-height: 100vh;
            height: auto;

            display: grid;
            grid: auto / 1fr;
            justify-content: center;
        }
        .text-justify{
            text-align: justify;
        }

        div{
            margin-top: 1.2em;
        }
        div:first-child{
            margin-top: 0px;
        }
        div:last-child{
            margin-bottom: .5em;
        }

        table{
            margin: 0 auto;
            max-width: 500px;
        }
        .img-tanque{
            max-width: 500px;
            width: 50%;
        }
        .img-logo{
            width: 100%;
            max-width: 750px;
        }
        .img-dibujo img{
            display: block;
            margin: 0 auto;
        }
        .cochito{
            max-width: 200px;
        }

        .text-center{
            text-align: center;
        }
        .bold{
            font-weight: 600;
        }
        p{
            margin: 0 auto;
            max-width: 700px;
            text-align: justify;
        }
        .table-cargos {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .table-cargos td, .table-cargos th {
            border: 1px solid rgba(255, 159, 64, 1);
            padding: 2px;
        }

        .table-cargos th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: rgba(255, 159, 64, 0.2);
            color: rgba(213, 120, 28, 0.9);
            font-weight: bolder;

        }

        .table-other {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .table-other td:first-child {
            padding-top: 5px;
            padding-bottom: 5px;
            text-align: left;
            background-color: rgba(255, 159, 64, 0.2);
            color: rgba(213, 120, 28, 0.9);
            font-weight: bolder;
        }

        .table-other td, .table-other th {
            border: 1px solid rgba(255, 159, 64, 1);
            padding: 2px;
        }



        .card{
            display: block;
            margin: 0 auto;
            border: 1px solid rgba(255, 159, 64, 1);
            max-width: 500px;
        }

        .card p{
            padding: 0px 15px 0px 15px;
            margin: 0px;
        }

        .socialLinks{
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="container">
        <div>
            <a class="links" href="#">
                <img  class="img-logo" src="http://testing.gaspasa.com.mx:8888/appsmkt/pruebas_correos/img/gaspasa/logo-lineas_1.png"/>
            </a>
            <p class="bold">
                Estimado(a): MARCIA JIMENEZ MORENO ROMERO
            </p>
            <p>
                Agradecemos su compra hecha el día 02 de agosto de 2021, por un importe de $1561.20. Le fueron surtidos
                117.60 litros de gas tratado con nuestro exclusivo aditivo, el cual mejora sustancialmente la combustión del
                gas brindandole considerables ahorros para beneficio de su economía.
            </p>
        </div>
        <div class="img-dibujo">
            <img class="img-tanque" src="http://testing.gaspasa.com.mx:8888/appsmkt/pruebas_correos/img/gaspasa/tanque.png" alt="">
            <p>
                De acuerdo a nuestro sistema de pagos, esta compra será cargada a su TARJETA DE CREDITO SANTANDER MEXICANO en 2 pagos, en las siguientes fechas:
            </p>
        </div>
        <div>
            <p style="text-align: center; padding: 0; line-height: 1rem;">SUMA PUNTOS</p><br>
            <table class="table-cargos" align="center">
                <thead>
                    <tr>
                        <th>Cargo</th>
                        <th>Fecha</th>
                        <th>Importe</th>
                        <th>Uso sistema</th>
                        <th>Total</th>
                    </tr>                                                    
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>29-10-2021</td>
                        <td>50.25</td>
                        <td>11.54</td>
                        <td>61.79</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>29-10-2021</td>
                        <td>50.25</td>
                        <td>11.54</td>
                        <td>61.79</td>
                    </tr>
                </tbody>
            </table>  
            <br>
            <p style="text-align: center; padding: 0; line-height: 1rem;">ESTADO DE CUENTA</p> <br>  
            <table class="table-other" align="center">
                <tbody>
                    <tr>
                        <td>Anteriores:</td>
                        <td>61.79</td>
                    </tr>
                    <tr>
                        <td>Se acumularan:</td>
                        <td>61.79</td>
                    </tr>
                    <tr>
                        <td>Saldo nuevo:</td>
                        <td>61.79</td>
                    </tr>
                </tbody>
            </table>  
        </div>
        <div>
            <div class="card">
                <p class="bold text-center">Puntos acumulados</p>
                <p>
                    Por esta compra usted acumulará 0 puntos más,
                    una vez que hayan sido realizados todos los
                    pagos oportunamente. Recuerde que al acumular
                    50 puntos los podrá canjear por 50 litros de 
                    ¡Gas Gratis!
                </p>
                <br>
                <div class="img-dibujo">
                    <img src="http://testing.gaspasa.com.mx:8888/appsmkt/pruebas_correos/img/gaspasa/cochito.png" alt="" class="cochito">
                </div>
            </div>
        </div>
        <div>
            <p class="msj-inicio">                                                                                                        
                De acuerdo a su consumo promedio diario de Gas, le surtiremos nuevamente el Jueves 25 de Marzo de 2021.
                Para saber más acerca de nuestro sistema SUMA PUNTOS, consulte nuestro siti web o síganos en Facebook. <br>
                Muchas gracias por su preferencia.
            </p>
        </div>
        <div>
            <div class="section-links" align="center">
                <div class="links">
                    <a class="socialLinks" href="https://www.facebook.com/GaspasaContigo" target="_blank">
                        <img class="socialIcon" src="http://testing.gaspasa.com.mx:8888/appsmkt/pruebas_correos/img/gaspasa/fb.png" width="60" />
                    </a>
                    <a class="socialLinks" href="https://www.instagram.com/gaspasa_contigo_siempre/" target="_blank">
                        <img class="socialIcon" src="http://testing.gaspasa.com.mx:8888/appsmkt/pruebas_correos/img/gaspasa/ig.png" width="60"/>
                    </a>
                    <a class="socialLinks" href="https://www.gaspasa.com.mx/" target="_blank">
                        <img class="socialIcon" src="http://testing.gaspasa.com.mx:8888/appsmkt/pruebas_correos/img/gaspasa/web.png" width="60"/>
                    </a>
                    <a class="socialLinks" href="https://wa.me/526691010101" target="_blank">
                        <img class="socialIcon" src="http://testing.gaspasa.com.mx:8888/appsmkt/pruebas_correos/img/gaspasa/whatsapp.png" width="60" />
                    </a>
                </div>                
            </div>    
        </div>
        <div>
            <canvas id="chartOne" width="200px" height="150px"></canvas>
        </div>
    </div>
       
</body>
<script type="text/javascript">
    const ctx = document.getElementById('chartOne');
    const medicion = 20;
    var vacio = 100 - medicion;

    const myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: [
                'nivel %',
            ],
            datasets: [{
                label: [
                    'Medición anterior', 
                ],
                data: [medicion, vacio],
                backgroundColor: [
                'rgb(54, 162, 235)',
                'gray',
                ],
                hoverOffset: 0,
                // rotation: -180 + (((100 - medicion) * 3.6)/2),
                rotation: -180,
                // circumference: 360 - ((100 - medicion) * 3.6),
                // circumference: 324,
            }]
        }
    });
</script> 
</html>