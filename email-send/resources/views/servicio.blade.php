<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Servicio de envío de correo</title>
    <style>
        body{
            font-family:'Arial Narrow', Arial, sans-serif;
            font-size: 3rem;
        }
    </style>
</head>
<body>
    <div>
        <p>Total de correos a enviar: <strong id="total" style="color:red">777</strong><br>enviados: <strong id="sended" style="color: blue">-1</strong> <br>restantes = <strong id="rest" style="color: orange">-1</strong> </p>
    </div>
    <script src="{{ asset('jquery.js') }}"></script>
    <script>
        const total = @json($data);
        let sended = 0, rest = total - sended;

        let inp_total = document.getElementById('total')
        let inp_sended = document.getElementById('sended')
        let inp_rest = document.getElementById('rest')

        inp_sended.innerHTML = sended;
        inp_rest.innerHTML = rest;
        inp_total.innerHTML = total;

        const total_lote = 10;
        const plaza = 1;

        window.onload = function(){

            setInterval(async function(){
            // for(var x=0; x<count_plazas;x++){
                
                // await new Promise(r => setTimeout(r, 1000));
                // datos.plazas_id = plazas[x];
                console.log('funcion ajax')
                await $.ajax({
                    url: "/test/"+plaza+"/"+total_lote, //url del server al cual le queres hacer la peticion
                    type : "GET", // post, get, merge .. tipos de peticiones http
                    datatype: "text", // formato de la respuesta
                    // data: datos,
                    success: function(response){
                        let res = JSON.parse(response);
                        sended += res.counter;
                        console.log(res);
                    }, // que hacer si la llamada se ejecuto bien 
                    error: function(error){
                        console.error(JSON.stringify(error));
                    }  //que hacer si la llamada dio algun error 
                });
            // }
            rest = total - sended;
            inp_sended.innerHTML = sended;
            inp_rest.innerHTML = rest;

            if(false){
                clearInterval(bucle);
            }
            },total_lote*500);
        }

    </script>
</body>
</html>