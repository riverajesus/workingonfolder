<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmailSendController;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/servicio', [TestController::class, 'servicio']);

Route::get('script-send', [TestController::class, 'script']);

Route::get('/test/{id}/{lote}', [TestController::class, 'index']);

Route::get('send-mail', [EmailSendController::class, 'index']);
