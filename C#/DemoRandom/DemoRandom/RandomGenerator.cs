﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRandom
{
    public class RandomGenerator
    {
        public static  string GenerateCode(int p_CodeLength = 0)
        {
            p_CodeLength = 36;
            string result = "";

            string pattern = "ABCDEFabcdef0123456789";
            Random myRndGenerator = new Random((int)DateTime.Now.Ticks);

            for (int i = 0; i < p_CodeLength; i++)
            {
                if (i == 8 || i == 13 || i == 18 || i == 23)
                {
                    result += "-";
                }
                else
                {
                    int mIndex = myRndGenerator.Next(pattern.Length);
                    result += pattern[mIndex];
                }

            }
            return result;
        }
    }
}
