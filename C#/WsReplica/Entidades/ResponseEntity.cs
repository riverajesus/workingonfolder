﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class ResponseEntity
    {
        public int ERROR { get; set; }
        public String MSG { get; set; }
    }
}
