﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entidades
{
    public class SetReportEntity
    {
        public string key { get; set; }
        public string archivo { get; set; }
        public Decimal inv_inicial { get; set; }
        public int entradas { get; set; }
        public int salidas { get; set; }
        public Decimal inv_final { get; set; }
        public Decimal inv_calculado { get; set; }
        public string url_xml { get; set; }
    }
}
