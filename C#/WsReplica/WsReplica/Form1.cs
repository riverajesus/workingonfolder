﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Newtonsoft.Json;

namespace WsReplica
{
    public partial class Form1 : Form
    {
        public ReportEntity entidad;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            txtStatus.Text = "Buscando...";
            //Task.Run(async () =>
            //{
            BusquedaAsync();
            //}).GetAwaiter().GetResult();
            txtStatus.Text = "Completado";

            //txtRespuesta.Text = JsonConvert.SerializeObject(entidad);
        }

        public String obtenerData()
        {
            ReportEntity resObj = new ReportEntity();
            resObj.id = 1;
            resObj.message = "Registros obtenidos con éxito";
            //return resObj;

            string resultado = "";
            string url = "http://www.sersi.com.mx:8082/api/lumenApi/setReportes";

            string requestUrl = url;
            string key = "ef73781effc5774100f87fe2f437a435";



            HttpWebRequest request = HttpWebRequest.CreateHttp(requestUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            //request.ContentType = "application/json";
            SetReportEntity json = new SetReportEntity();
            json.key = "2500";
            json.archivo = "archivo de reporte";
            json.inv_inicial = Decimal.Parse("185.00");
            json.entradas = 2;
            json.salidas = 21;
            json.inv_final = Decimal.Parse("125.20");
            json.inv_calculado = Decimal.Parse("109.34");
            json.url_xml = "www.sersi.com.mx/xml/descargar/2500";

            String str_json = JsonConvert.SerializeObject(json);

            str_json = "key=" + json.key;
            str_json += "&archivo=" + json.archivo;
            str_json += "&inv_inicial=" + json.inv_inicial;
            str_json += "&entradas=" + json.entradas;
            str_json += "&salidas=" + json.salidas;
            str_json += "&inv_final=" + json.inv_final;
            str_json += "&inv_calculado=" + json.inv_calculado;
            str_json += "&url_xml=" + json.url_xml;

            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(str_json);
            }

            // Submit the request, and get the response body from the remote server.
            string responseFromRemoteServer;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    responseFromRemoteServer = reader.ReadToEnd();
                    resultado = responseFromRemoteServer;
                }
            }

            try
            {
                string myDeserializedClass = resultado;
                ResponseEntity repEntity = JsonConvert.DeserializeObject<ResponseEntity>(resultado);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al deserializar: " + ex);
            }
            return resultado;
        }

        public String BusquedaAsync()
        {
            string res = "";
           Console.WriteLine("Buscando...");

            res = obtenerData();

            Console.WriteLine("completado");

            return res;
        }
    }
}
