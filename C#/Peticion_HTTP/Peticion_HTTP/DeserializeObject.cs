﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peticion_HTTP
{
    class DeserializeObject
    {
        public class InfoPlaza
        {
            public string id { get; set; }
            public string nombre { get; set; }
            public string plaza { get; set; }
            public string ciudad { get; set; }
            public string tel_plaza { get; set; }
            public string tel_pedidos { get; set; }
            public string tel_quejas { get; set; }
        }

        public class Root
        {
            public int Error { get; set; }
            public List<InfoPlaza> info_plazas { get; set; }

            public Root()
            {
                this.Error = 0;
                this.info_plazas = new List<InfoPlaza>();
            }
        }

    }
}
