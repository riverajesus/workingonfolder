﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peticion_HTTP
{
    public class RequestBody
    {
        public Credentials credentials;
        public String serie;
        public String folio_inicial;
        public String folio_final;
        public String mes;
        public String anio;
        public String rfc;
        public String sucursal_id;
    }

    public class Credentials
    {
        public String id;
        public String token;
    }
}
