﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Peticion_HTTP
{
    public partial class Form1 : Form
    {
        DeserializeObject.Root myDeserializedClass;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            txtResultado.Text = EjecutarPeticion(txtURL.Text.ToString());

        }

        private String EjecutarPeticion(String url)
        {
            String resultado = "Absolutamente nada";
            string requestUrl = url;
            string key = "ef73781effc5774100f87fe2f437a435";



            HttpWebRequest request = HttpWebRequest.CreateHttp(requestUrl);
            request.Method = "POST";

            // Optionally, set properties of the HttpWebRequest, such as:
            //request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            request.ContentType = "application/x-www-form-urlencoded";
            // Could also set other HTTP headers such as Request.UserAgent, Request.Referer,
            // Request.Accept, or other headers via the Request.Headers collection.

            // Set the POST request body data. In this example, the POST data is in 
            // application/x-www-form-urlencoded format.
            //string postData = "myparam1=myvalue1&myparam2=myvalue2";
            string postData = "key="+ key;
            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(postData);
            }

            // Submit the request, and get the response body from the remote server.
            string responseFromRemoteServer;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    responseFromRemoteServer = reader.ReadToEnd();
                    resultado = responseFromRemoteServer;
                }
            }

            try
            {
                myDeserializedClass = JsonConvert.DeserializeObject<DeserializeObject.Root>(resultado);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al deserializar: "+ex);
            }

            return resultado;
        }

        private String EjecutarPeticionKonesh(String url)
        {
            String resultado = "Absolutamente nada";
            string requestUrl = "https://10.1.1.220/KServiceAPI/webresources/WSConsultaCFDI/request";
            string key = "ef73781effc5774100f87fe2f437a435";

            RequestBody requestBody = new RequestBody();
            requestBody.credentials = new Credentials();
            requestBody.credentials.id = "1";
            requestBody.credentials.token = "77d2c09e6ee57f81b52b3cc04de94b77";
            requestBody.serie = "";
            requestBody.folio_inicial = "1";
            requestBody.folio_final = "121";
            requestBody.mes = "6";
            requestBody.anio = "2022";
            requestBody.rfc = "XAXX010101000";
            requestBody.sucursal_id = "1";

            string json = JsonConvert.SerializeObject(requestBody);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = HttpWebRequest.CreateHttp(requestUrl);
            request.Method = "POST";

            // Optionally, set properties of the HttpWebRequest, such as:
            //request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            //request.ContentType = "application/x-www-form-urlencoded";
            request.ContentType = "application/json";
            // Could also set other HTTP headers such as Request.UserAgent, Request.Referer,
            // Request.Accept, or other headers via the Request.Headers collection.

            // Set the POST request body data. In this example, the POST data is in 
            // application/x-www-form-urlencoded format.
            //string postData = "myparam1=myvalue1&myparam2=myvalue2";
            //string postData = "key=" + key;
            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(json);
            }

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            // Submit the request, and get the response body from the remote server.
            string responseFromRemoteServer;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    responseFromRemoteServer = reader.ReadToEnd();
                    resultado = responseFromRemoteServer;
                }
            }

            try
            {
                myDeserializedClass = JsonConvert.DeserializeObject<DeserializeObject.Root>(resultado);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al deserializar: " + ex);
            }

            return resultado;
        }

        private void btnKonesh_Click(object sender, EventArgs e)
        {
            txtResultado.Text = EjecutarPeticionKonesh(txtURL.Text.ToString());
        }

    }
}
