﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Peticion_HTTP
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Obtener zona horaria
            TimeZoneInfo localZone = TimeZoneInfo.Local;
            string zonaHoraria = localZone.BaseUtcOffset.ToString();
            Console.WriteLine(zonaHoraria);
            zonaHoraria.Substring(0, 6);
            Console.WriteLine(zonaHoraria);

            String clavePermiso = generarClavePermiso("LP/15135/EXP/ES/2016");
            //String clavePermiso = generarClavePermiso("SENER-REF-XXX-AAAA");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
        public static String generarClavePermiso(String numPermiso)
        {
            String clave = "";
            int lgPermiso = numPermiso.Length;

            clave = numPermiso.Remove(lgPermiso - 4, 4);
            clave += "AAAA";

            string pattern = "[0-9]|5";
            clave = Regex.Replace(clave, pattern, "X");
            return clave;
        }
    }
}
