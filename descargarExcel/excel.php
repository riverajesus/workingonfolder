<?php
    session_start();
    if(isset($_POST)){
        if (!isset($_SESSION['estatus'])) {
            header("location:index.php");
            exit;
        }else if($_SESSION["estatus"] === 0) {
            header("location:index.php");
            exit;
        }
        header('Content-type: application/vnd.ms-excel;charset=iso-8859-15');
        header('Content-Disposition: attachment; filename=servicios.xls');

        require_once'app/conexion.php';

        $con = abrirConexion();

        if(trim($_POST['busqueda']) == "" && (trim($_POST['fromDate']) == "" || trim($_POST['toDate']) == "")){
            $query="SELECT servicios.num_servicio, servicios.litros, servicios.hora_surtido, vehiculos.codigo,vehiculos.descripcion,servicios.fecha_surtido FROM servicios INNER JOIN vehiculos ON servicios.vehiculos_id = vehiculos.id WHERE servicios.litros != 0 AND vehiculos.estaciones_id=".$_SESSION['estaciones_id']." ORDER BY servicios.fecha_surtido DESC, servicios.num_servicio ASC;";
            //echo $query;
        }
        else if(trim($_POST['busqueda']) != "" && (trim($_POST['fromDate']) == "" && trim($_POST['toDate']) == "") ){
            $busqueda = $_POST['busqueda'];
            $query='SELECT servicios.num_servicio, servicios.litros, servicios.hora_surtido, vehiculos.codigo,vehiculos.descripcion, servicios.fecha_surtido
            FROM servicios INNER JOIN vehiculos ON servicios.vehiculos_id = vehiculos.id
            WHERE  servicios.litros != 0 AND vehiculos.estaciones_id='.$_SESSION['estaciones_id'].'
            AND (servicios.num_servicio LIKE "%'.$busqueda.'%" 
            OR servicios.litros LIKE "%'.$busqueda.'%"
            OR vehiculos.codigo LIKE "%'.$busqueda.'%"
            OR servicios.hora_surtido LIKE "%'.$busqueda.'%"
            OR vehiculos.descripcion LIKE "%'.$busqueda.'%") 
            ORDER BY servicios.fecha_surtido DESC, servicios.num_servicio ASC;';
        }
        else if(trim($_POST['fromDate']) != "" && trim($_POST['toDate'] != "")){
            $fromDate = date('Y-m-d',strtotime($_POST['fromDate']));
            $toDate = date('Y-m-d',strtotime($_POST['toDate']));

            $query = 'SELECT servicios.num_servicio, servicios.litros, servicios.hora_surtido, vehiculos.codigo,vehiculos.descripcion, servicios.fecha_surtido
            FROM servicios INNER JOIN vehiculos ON servicios.vehiculos_id = vehiculos.id
            WHERE  servicios.litros != 0 AND servicios.fecha_surtido BETWEEN "'.$fromDate.'" AND "'.$toDate.'" AND vehiculos.estaciones_id='.$_SESSION['estaciones_id'].' 
            ORDER BY servicios.fecha_surtido DESC, servicios.num_servicio ASC;';
        }else{
            echo '<script>alert("Error en el envio de los parametros"); window.location.href = "servicios.php"</script>';
        }
        $result = mysqli_query($con,$query);
        
    }

?>
<style>
    th, td 
    {
        border: 1px solid #ddd;
        
    }
}
</style>
<table cellspacing="0">
    <thead>
        <tr><td colspan="6" style="text-align:center; border:0px; font-size:20px;">Listado de servicios</td><tr>
        <tr>
            <th style="background-color:#BE202F; color:white; width: 150px; height: 30px;">N&uacute;mero servicio</th>
            <th style="background-color:#BE202F; color:white; width: 150px; height: 30px;">Litros surtidos</th>
            <th style="background-color:#BE202F; color:white; width: 150px; height: 30px;">Hora</th>
            <th style="background-color:#BE202F; color:white; width: 150px; height: 30px;">C&oacute;digo Veh&iacute;culo</th>
            <th style="background-color:#BE202F; color:white; width: 150px; height: 30px;">Descripci&oacute;n</th>
            <th style="background-color:#BE202F; color:white; width: 150px; height: 30px;">Fecha</th>
        </tr>
    </thead>
    <?php   
        while($row = mysqli_fetch_assoc($result))
        {
    ?>
            <tr>
                <td style="background-color: #f2f2f2; height: 20px; text-align:center;"><?php echo $row['num_servicio']; ?></td>
                <td style="background-color: #f2f2f2; height: 20px; text-align:center;"><?php echo $row['litros']; ?></td>
                <td style="background-color: #f2f2f2; height: 20px; text-align:center;"><?php echo $row['hora_surtido']; ?></td>
                <td style="background-color: #f2f2f2; height: 20px; text-align:center;"><?php echo $row['codigo']; ?></td>
                <td style="background-color: #f2f2f2; height: 20px; text-align:center;"><?php echo $row['descripcion']; ?></td>
                <td style="background-color: #f2f2f2; height: 20px; text-align:center;"><?php echo $row['fecha_surtido']; ?></td>
            </tr>
    <?php
        }
    ?>
</table>