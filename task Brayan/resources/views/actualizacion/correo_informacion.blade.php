<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Correo</title> 
        <style>

            .socialIcon{
                outline: none;
                text-decoration: none;
                -ms-interpolation-mode: bicubic;
                border: none;
                text-align: center;
                display: block;
                /* width: 50%; */
                margin:auto;
            }
            .donwload1{
                display: block;
                /* width: 75%; */
                margin:auto;
            }
            .donwload2{
                display: block;
                /* width: 76%; */
                margin:auto;
            }
            .donwload3{
                display: block;
                /* width: 85%; */
                margin:auto;
            }
            .links{
               text-decoration: none;
            }
            #mainTable{
                /* display: table;
                border-collapse: separate;
                box-sizing: border-box;
                text-indent: initial;
                border-spacing: 2px;
                border-color: grey; */
                width: 100%;
                border-top-width: 0px;
                border-right-width: 0px;
                border-bottom-width: 0px;
                border-left-width: 0px;
                -webkit-border-horizontal-spacing: 0px;
                -webkit-border-vertical-spacing: 0px;

                display: table;
                border-collapse: separate;
                box-sizing: border-box;
                text-indent: initial;
                border-spacing: 2px;
                border-color: grey;
                line-height: normal !important;

            }
            
            .logo{
                width: 260px;
                height: 150px;
                margin: 0; 
                padding: 0; 
                outline: none; 
                text-decoration: none; 
                -ms-interpolation-mode: bicubic; 
                border: none; 
                display: block;
            }
            .button-gaspasa {
               background-color: #f4ab0b;
               border: none;
               color: white;
               padding: 8px 15px;
               text-align: center;
               text-decoration: none;
               display: inline-block;
               margin: 4px 4px;
               cursor: pointer;
               width: 95%;
               font-family: sans-serif;
             }
            
             .button-diesgas{
                background-color: #F63440;
                border: none;
                color: white;
                padding: 8px 15px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                margin: 4px 4px;
                cursor: pointer;
                width: 95%;
                font-family: sans-serif;
             }
            
             .button-caligas{
                background-color: #F63440;
                border: none;
                color: white;
                padding: 8px 15px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                margin: 4px 4px;
                cursor: pointer;
                width: 95%;
                font-family: sans-serif;
             }
            
             body{
                margin: 0;
                padding: 0;
                background: #ecf2f7;
                width: 100% !important;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                line-height: normal !important;
             }
             .tdTable{
                border-collapse: collapse; 
                border-spacing: 0; 
                margin: 0; 
                padding: 0;
            }
            .table-2
            {
                border-collapse: collapse; 
                border-spacing: 0;
                padding: 0;
                background: white;
                

                
                border-top-width: 0px;
                border-right-width: 0px;
                border-bottom-width: 0px;
                border-left-width: 0px;
                margin-inline-start: auto;
                margin-inline-end: auto;
                -webkit-border-horizontal-spacing: 0px;
                -webkit-border-vertical-spacing: 0px;
            }
            .tr-span{
                background: #B9C2CA;font-family: sans-serif;
            }
            .td-span{
                vertical-align:middle;
            }
            .mt-mb-15
            {
                margin-top: 15px; 
                margin-bottom: 15px;
            }
            tbody{
                display: table-row-group;
                vertical-align: middle;
                border-color: inherit;
            }
        </style>
    </head>
    <body  style="overflow:hidden">
        
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <table id="mainTable">
            <tr>
                <td align="center" valign="top" class="tdTable">
                    <table border="0"  cellpadding="0" cellspacing="0" style="width: 646px;" class="table-2">
                        <tr style="height:45px;" class="tr-span">
                            <td align="center" align="center" valign="top" style="font-family:Arial, sans-serif, Helvetica;" class="td-span">
                                    <span style="font-size:11px;">Si no puedes ver las imágenes de este correo da <a target="_blank" href="{{url('/test2/'.$msg['id'])}}">clic aquí</a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" class="tdTable" >
                            <!-- LOGO -->
                            <a class="links" href="#">
                                <img src="{{url($msg['icono'])}}" width="150" heigth="300" style="margin-bottom: 0.2em;"/>
                            </a><br>
                            </td>
                        </tr>
                        <tr>
                        <td align="center" valign="top" class="tdTable">
                            <a class="links" href="#">
                                <img src="{{url($msg['logoGral'])}}" width="646" heigth="300"/>
                            </a><br>
                        </td>
                        </tr>
                        <tr>
                        <td align="center" valign="top" style="font-size: 18px;font-weight: bold;color: #7B7B7B;font-family: Arial, sans-serif, Helvetica;" class="tdTable">
                            <br>  
                            <b style="margin-top:25px">Solicita gas y paga en línea desde tu App, además puedes consultar tus consumos y estar al día con nuestras promociones.</b>
                        </td>
                        </tr>
    
                        <tr>
                        <td align="center" valign="top" class="tdTable">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a style="float: left;" href="#" target="_blank">
                                                <img class="donwload1" src="{{url($msg['playstore'])}}" width="150" heigth="300" />
                                            </a>
                                        </td>
                                        <td>
                                            <a style="float: left;" href="{{$msg['url-appstore']}}" target="_blank">
                                                <img class="donwload2" style="" src="{{url($msg['appstore'])}}" width="115" heigth="300"/>
                                            </a>
                                        </td>
                                        <td>
                                            <a style="float: left;" href="{{$msg['url-appgallery']}}" target="_blank">
                                                <img class="donwload3" src="{{url($msg['appgallery'])}}" width="150" heigth="300" />
                                            </a>
                                        </td>   
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        </tr>
    
                        <tr>
                        <td align="center" valign="top" style="font-size: 15px;color: #7B7B7B;font-family: Arial, sans-serif, Helvetica;" class="tdTable">
                            <div style="margin-top: 15px;">Descarga la última actualización de tu App y disfruta de nuevas funcionalidades.</div>
                        </td>
                        </tr>
    
                        <tr>
                        <td align="center" valign="top" style="font-size: 13px;font-weight: bold;color: #0051A1;font-family: Arial, sans-serif, Helvetica;" class="tdTable">
                            <div class="mt-mb-15"><b>Síguenos en redes sociales</b></div>
                        </td>
                        </tr>

                        <!-- Redes sociales -->
                        <tr>
                        <td align="center" valign="top" class="tdTable">
                            <table style="margin-bottom:0.5em;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlFb']}}" target="_blank">
                                                <img class="socialIcon" src="{{url($msg['imgFb'])}}" width="50" heigth="300" />
                                            </a>
                                        </td>
                                        @if($msg['urlIg'] != "")
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlIg']}}" target="_blank">
                                                <img class="socialIcon" src="{{url($msg['imgIg'])}}" width="50" heigth="300"/>
                                            </a>
                                        </td>
                                        @endif
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlWeb']}}" target="_blank"> 
                                                <img class="socialIcon" src="{{url($msg['imgWeb'])}}" width="50" heigth="300"/>
                                            </a>
                                        </td>
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlWs']}}" target="_blank"> 
                                                <img class="socialIcon" src="{{url($msg['imgWs'])}}" width="50" heigth="300"/>
                                            </a> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        </tr>
                        
                        <tr style="height:45px;" class="tr-span" >
                            <td align="center" style="font-family:Arial, sans-serif, Helvetica;" class="td-span">
                                    <span style="font-size:11px;">
                                        Agrega <a target="_blank" href="mailto:ayuda@sersi.com.mx">ayuda@sersi.com.mx</a>  a tus contactos
                                    </span>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
        @endif
        

        @if((new \Jenssegers\Agent\Agent())->isMobile())
        
        <table id="mainTable">
            <tr>
                <td align="center" valign="top" class="tdTable">
                    <table border="0"  cellpadding="0" cellspacing="0" style="width: 320px;" class="table-2">
                        <tr style="height:30px;" class="tr-span">
                            <td align="center" align="center" valign="top" style="font-family:Arial, sans-serif, Helvetica;" class="td-span">
                                    <span style="font-size:10px;">Si no puedes ver las imágenes de este correo da <a target="_blank" href="{{url('/test2/'.$msg['id'])}}">clic aquí</a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" class="tdTable" >
                            <!-- LOGO -->
                            <a class="links" href="#">
                                <img src="{{url($msg['icono'])}}" width="90" heigth="300" style="margin-bottom: 0.2em;"/>
                            </a><br>
                            </td>
                        </tr>
                        <tr>
                        <td align="center" valign="top" class="tdTable">
                            <a class="links" href="#">
                                <img src="{{url($msg['logoGral'])}}" width="320" heigth="300"/>
                            </a><br>
                        </td>
                        </tr>
                        <tr>
                        <td align="center" valign="top" style="font-size: 13px;font-weight: bold;color: #7B7B7B;font-family: Arial, sans-serif, Helvetica;" class="tdTable">
                            <br>  
                            <b style="margin-top:25px">Solicita gas y paga en línea desde tu App, además puedes consultar tus consumos y estar al día con nuestras promociones.</b>
                        </td>
                        </tr>
    
                        <tr>
                        <td align="center" valign="top" class="tdTable">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a style="float: left;" href="#" target="_blank">
                                                <img class="donwload1" src="{{url($msg['playstore'])}}" width="85" heigth="300" />
                                            </a>
                                        </td>
                                        <td>
                                            <a style="float: left;" href="{{$msg['url-appstore']}}" target="_blank">
                                                <img class="donwload2" style="" src="{{url($msg['appstore'])}}" width="64" heigth="300"/>
                                            </a>
                                        </td>
                                        <td>
                                            <a style="float: left;" href="{{$msg['url-appgallery']}}" target="_blank">
                                                <img class="donwload3" src="{{url($msg['appgallery'])}}" width="85" heigth="300" />
                                            </a>
                                        </td>   
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        </tr>
    
                        <tr>
                        <td align="center" valign="top" style="font-size: 11px;color: #7B7B7B;font-family: Arial, sans-serif, Helvetica;" class="tdTable">
                            <div style="margin-top: 15px;">Descarga la última actualización de tu App y disfruta de nuevas funcionalidades.</div>
                        </td>
                        </tr>
    
                        <tr>
                        <td align="center" valign="top" style="font-size: 11px;font-weight: bold;color: #0051A1;font-family: Arial, sans-serif, Helvetica;" class="tdTable">
                            <div class="mt-mb-15"><b>Síguenos en redes sociales</b></div>
                        </td>
                        </tr>

                        <!-- Redes sociales -->
                        <tr>
                        <td align="center" valign="top" class="tdTable">
                            <table style="margin-bottom:0.5em;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlFb']}}" target="_blank">
                                                <img class="socialIcon" src="{{url($msg['imgFb'])}}" width="25" heigth="300" />
                                            </a>
                                        </td>
                                        @if($msg['urlIg'] != "")
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlIg']}}" target="_blank">
                                                <img class="socialIcon" src="{{url($msg['imgIg'])}}" width="25" heigth="300"/>
                                            </a>
                                        </td>
                                        @endif
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlWeb']}}" target="_blank"> 
                                                <img class="socialIcon" src="{{url($msg['imgWeb'])}}" width="25" heigth="300"/>
                                            </a>
                                        </td>
                                        <td>
                                            <a style="float: left;" href="{{$msg['urlWs']}}" target="_blank"> 
                                                <img class="socialIcon" src="{{url($msg['imgWs'])}}" width="25" heigth="300"/>
                                            </a> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        </tr>
                        
                        <tr style="height:30px;" class="tr-span" >
                            <td align="center" style="font-family:Arial, sans-serif, Helvetica;" class="td-span">
                                    <span style="font-size:10px;">
                                        Agrega <a target="_blank" href="mailto:ayuda@sersi.com.mx">ayuda@sersi.com.mx</a>  a tus contactos
                                    </span>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
        
        @endif
    
        
    </body>
</html>