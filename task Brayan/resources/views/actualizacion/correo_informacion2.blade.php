<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <title>Correo</title> 
        <style>

            .socialIcon{
                outline: none;
                text-decoration: none;
                -ms-interpolation-mode: bicubic;
                border: none;
                text-align: center;
                display: block;
                width: 50%;
                margin:auto;
            }
            .donwload1{
                display: block;
                width: 75%;
                margin:auto;
            }
            .donwload2{
                display: block;
                width: 76%;
                margin:auto;
            }
            .donwload3{
                display: block;
                width: 85%;
                margin:auto;
            }
            .links{
               text-decoration: none;
            }
            #mainTable{
                display: table;
                border-collapse: separate;
                box-sizing: border-box;
                text-indent: initial;
                border-spacing: 2px;
                border-color: grey;
            }
            
            .logo{
                width: 260px;
                height: 150px;
                margin: 0; 
                padding: 0; 
                outline: none; 
                text-decoration: none; 
                -ms-interpolation-mode: bicubic; 
                border: none; 
                display: block;
            }
            .button-gaspasa {
               background-color: #f4ab0b;
               border: none;
               color: white;
               padding: 8px 15px;
               text-align: center;
               text-decoration: none;
               display: inline-block;
               margin: 4px 4px;
               cursor: pointer;
               width: 95%;
               font-family: sans-serif;
             }
            
             .button-diesgas{
                background-color: #F63440;
                border: none;
                color: white;
                padding: 8px 15px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                margin: 4px 4px;
                cursor: pointer;
                width: 95%;
                font-family: sans-serif;
             }
            
             .button-caligas{
                background-color: #F63440;
                border: none;
                color: white;
                padding: 8px 15px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                margin: 4px 4px;
                cursor: pointer;
                width: 95%;
                font-family: sans-serif;
             }
            
             body{
                margin: 0;
                padding: 0;
                background: #ecf2f7;
                -webkit-text-size-adjust: none;
             }
             .tdTable{
                border-collapse: collapse; 
                border-spacing: 0; 
                margin: 0; 
                padding: 0;
            }
            .table-2
            {
                border-collapse: collapse; 
                border-spacing: 0;
                padding: 0;
                width: inherit;
                background: white;
                width: 50%;
            }
            .tr-span{
                background: #B9C2CA; height:5vh;font-family: sans-serif;
            }
            .td-span{
                vertical-align:middle;
            }
            span{
                font-size:70%;
            }
            .text-1
            {
                font-size: 18px;
                font-weight: bold;
                color: #7B7B7B;
                font-family: sans-serif;
            }
            .text-2
            {
                font-size: 15px; 
                color: #7B7B7B;
                font-family: sans-serif;
            }
            .text-3
            {
                font-size: 13px;
                font-weight: bold;
                color: #0051A1;
                font-family: sans-serif;
            }
            .mt-mb-15
            {
                margin-top: 15px; 
                margin-bottom: 15px;
            }
        </style>
    </head>
    <body> 
       <table id="mainTable">
        <tr>
            <td align="center" valign="top" class="tdTable">
                <table border="0"  cellpadding="0" cellspacing="0" class="table-2">
                    <tr class="tr-span">
                        <td align="center" class="td-span">
                                <span>Si no puedes ver las imágenes de este correo da <a target="_blank" href="{{url('/test2/'.$msg['id'])}}">clic aquí</a>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="tdTable" >
                        <!-- LOGO -->
                        <a class="links" href="#">
                            <img src="{{url($msg['icono'])}}"  style="margin-bottom: 0.2em;width:25%"/>
                        </a><br>
                        </td>
                    </tr>
                    <tr>
                      <td align="center" valign="top" class="tdTable" style="">
                     
                      <a class="links" href="#">
                          <img src="{{url($msg['logoGral'])}}" width="100%" style="margin-bottom: 1em;"/>
                      </a><br>
                      </td>
                    </tr>
  
                    <tr>
                      <td align="center" valign="top" class="tdTable text-1">
                        <b>Solicita gas y paga en línea desde tu App, además puedes consultar tus consumos y estar al día con nuestras promociones.</b>
                      </td>
                    </tr>
  
                    <tr>
                      <td align="center" valign="top" class="tdTable">
                         <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <a style="float: left;" href="#" target="_blank">
                                            <img class="donwload1" src="{{url($msg['playstore'])}}" width="auto" />
                                        </a>
                                    </td>
                                    <td>
                                        <a style="float: left;" href="#" target="_blank">
                                            <img class="donwload2" style="" src="{{url($msg['appstore'])}}" width="auto" />
                                        </a>
                                    </td>
                                    <td>
                                        <a style="float: left;" href="{{$msg['url-appgallery']}}" target="_blank">
                                            <img class="donwload3" src="{{url($msg['appgallery'])}}" width="auto" />
                                        </a>
                                    </td>   
                                </tr>
                            </tbody>
                        </table>
                      </td>
                    </tr>
  
                    <tr>
                      <td align="center" valign="top" class="tdTable text-2">
                        <div style="margin-top: 15px;">Descarga la última actualización de tu App y disfruta de nuevas funcionalidades.</div>
                      </td>
                    </tr>
  
                    <tr>
                      <td align="center" valign="top" class="tdTable text-3">
                        <div class="mt-mb-15"><b>Síguenos en redes sociales</b></div>
                      </td>
                    </tr>

                     <!-- Redes sociales -->
                    
                    <tr>
                      <td align="center" valign="top" class="tdTable">
                        <table style="margin-bottom:0.5em;">
                            <tbody>
                                <tr>
                                    <td>
                                        <a style="float: left;" href="{{$msg['urlFb']}}" target="_blank">
                                            <img class="socialIcon" src="{{url($msg['imgFb'])}}" width="auto" />
                                        </a>
                                    </td>
                                    <td>
                                        <a style="float: left;" href="{{$msg['urlIg']}}" target="_blank">
                                            <img class="socialIcon" src="{{url($msg['imgIg'])}}" width="auto"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a style="float: left;" href="{{$msg['urlWeb']}}" target="_blank"> 
                                            <img class="socialIcon" src="{{url($msg['imgWeb'])}}" width="auto"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a style="float: left;" href="{{$msg['urlWs']}}" target="_blank"> 
                                            <img class="socialIcon" src="{{url($msg['imgWs'])}}" width="auto"/>
                                        </a> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                      </td>
                    </tr>
                    
                    <tr class="tr-span" >
                        <td align="center" class="td-span">
                                <span>
                                    Agrega <a target="_blank" href="mailto:ayuda@sersi.com.mx">ayuda@sersi.com.mx</a>  a tus contactos
                                </span>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
      </table>
    </body>
</html>