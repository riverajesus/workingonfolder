<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0;">
        <meta name="format-detection" content="telephone=no" />
       
        <link href="//db.onlinewebfonts.com/c/58d1dea01043374550524409ab767ffb?family=Uniform-Medium" rel="stylesheet" type="text/css"/>
        <link href="//db.onlinewebfonts.com/c/3f0790dc920e46e8d9c39ee11c90cd25?family=Uniform" rel="stylesheet" type="text/css"/>
        <!-- MESSAGE SUBJECT -->
        <title>Condominios</title>
        <style>
            .table-container{
                border-collapse: collapse; 
                border-spacing: 0; 
                padding: 0; 
                width: inherit;
                background-color: white;
            }
            .table{
                border-collapse: collapse; 
                border-spacing: 0; 
                padding: 0; 
                width: 100%;
                max-width: 800px;
                background-color: white;
            }
            .td{
                margin: 0; 
                padding-top: 25px; 
                line-height: 2.1em;  
                font-family: sans-serif;
            }
            .socialIcon{
                padding: 0;
                outline: none;
                text-decoration: none;
                -ms-interpolation-mode: bicubic;
                border: none;
                display: block;
                cursor: inherit;
            }
            .socialLinks{
                display: block;
                margin: 0 10px 0 10px;
                text-decoration: none;
            }
            .socialLinks img{
                max-width: 60px;
                width: 100%;
                cursor: pointer;
            }
            #mainTable{
                border-collapse: collapse; 
                border-spacing: 0; 
                margin: 0 auto; 
                padding: 0; 
                /* width: 800px; */
            }
            .logo{
                width: 260px;
                height: 150px;
                margin: 0; 
                padding: 0; 
                outline: none; 
                text-decoration: none; 
                -ms-interpolation-mode: bicubic; 
                border: none; 
                display: block;
            }
            body{
                font-family: sans-serif; 
                font-size: 18px !important; 
                line-height: 1.4em; 
                /* color: #135eac;   */
                color: #000;
            }
            p {
                color: #000;
                line-height: 1.7rem;
                text-align: justify;
            }
            .tr-2{
                display: flex;
                justify-content: space-around;
                width: 100%;
                flex-wrap: wrap;
            }
            .tr-2 > td.td3{
                width: 33.33333%;
                padding: 0px;
                margin: 0px;
                /* background-color: gray; */
                text-align: center;
                /* border: solid 2px black; */
            }
            .tr-2 > td.td2{
                width: 50%;
                padding: 0px;
                margin: 0px;
                /* background-color: gray; */
                text-align: center;
                overflow: hidden;
            }
            .tr-2 > td.td1{
                width: 55%;
                padding: 0px;
                margin: 0px;
                text-align: center;
                overflow: hidden;
                display: flex;
                justify-content: center;
            }

            .table-cargos {
                font-family: Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                font-size: 16px;
            }

            .table-cargos td, .table-cargos th {
                border: 1px solid rgba(255, 159, 64, 1);
                padding: 2px;
            }

            /* .table-cargos tr:nth-child(even){background-color: #f2f2f2;} */

            /* .table-cargos tr:hover {background-color: #ddd;} */

            .table-cargos th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                /* background-color: #EC880E; */
                background-color: rgba(255, 159, 64, 0.2);
                color: rgba(213, 120, 28, 0.9);
                font-weight: bolder;

            }

            .table-other {
                font-family: Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
                font-size: 16px;
            }

            .table-other td:first-child {
                padding-top: 5px;
                padding-bottom: 5px;
                text-align: left;
                background-color: rgba(255, 159, 64, 0.2);
                color: rgba(213, 120, 28, 0.9);
                font-weight: bolder;
            }

            .table-other td, .table-other th {
                /* border: 1px solid #ddd;
                padding: 2px; */
                /* background-color: rgba(255, 159, 64, 0.2);
                color: rgba(213, 120, 28, 0.9); */
                border: 1px solid rgba(255, 159, 64, 1);
                padding: 2px;
            }

            .h4{
                margin: 0px;
                padding: 0px;
                margin-bottom: 5px;
            }

            .card{
                margin-left: 15px;
                border: 1px solid rgba(255, 159, 64, 1);
            }

            .card p{
                font-size: 17px;
                padding: 0px 15px 0px 15px;
                margin: 0px;
            }

            /* canvas {
                border: solid 1px black ;
            } */
            @media (max-width: 379px) {
                .tr-2 > td.td3{
                    width: 60%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td3.tanque{
                    width: 70%;
                }
                .tr-2 > td.td2{
                    width: 90%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td2.p{
                    width: 100%;
                }
                .tr-2 > td.td1{
                    width: 100%;
                }
                .card{
                    margin: 0px;
                }
                p {
                    font-size: 1.0rem;
                    line-height: normal;
                }
                span {
                    font-size: 1.0rem;
                    line-height: normal;
                }
                .card p{
                    font-size: 1rem;
                }
            }
            @media (min-width: 380px) and (max-width: 455px) {
                .tr-2 > td.td3{
                    width: 50%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td3.tanque{
                    width: 60%;
                }
                .tr-2 > td.td2{
                    width: 87%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td2.p{
                    width: 100%;
                }
                .tr-2 > td.td1{
                    width: 100%;
                }
                .card{
                    margin: 0px;
                }
                p {
                    font-size: 1.0rem;
                    line-height: normal;
                }
                span {
                    font-size: 1.0rem;
                    line-height: normal;
                }
                .card p{
                    font-size: 1rem;
                }
            }

            @media (min-width: 456px) and (max-width: 575px) {
                .tr-2 > td.td3{
                    width: 50%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td2{
                    width: 80%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td2.p{
                    width: 100%;
                }
                .card{
                    margin: 0px;
                }
                p {
                    font-size: 1.1rem;
                }
                span {
                    font-size: 1.1rem;
                }
                .card p{
                    font-size: 1.1rem;
                }
                
            }

            @media (min-width: 576px) and (max-width: 767px) {
                .tr-2 > td.td3{
                    width: 40%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td2{
                    width: 60%;
                    margin: 1em 0 1em 0;
                }
                .tr-2 > td.td2.p{
                    width: 100%;
                }
                .card{
                    margin: 0px;
                }
                
                .card p{
                    font-size: 17px;
                    padding: 0px 15px 0px 15px;
                    margin: 0px;
                }
            }

            @media (min-width: 768px) and (min-width: 991px) {
                .table{
                    width: 100%;
                    max-width: 800px;
                }
            }

            @media (min-width: 992px) and (min-width: 1199px) {
                .table{
                    width: 100%;
                    max-width: 800px;
                }
                
            }

            @media (min-width: 1200px) {
                .table{
                    width: 100%;
                    max-width: 800px;
                }
            }
        </style>    
    </head>

    <body >
        <table id="mainTable">
            <tr>
                <td >

                    <table class="table-container">

                        <tr>
                            <td align="center">

                            <!-- LOGO -->
                            <a class="links" href="{{ url('/') }}">
                                <img src="{{url($msg['lineas'])}}"/>
                            </a><br>
                            </td>
                        </tr>

                        <!-- LIST -->
                        <tr>
                            <td align="center">
                                <table class="table">
                                    <!-- Datos del pago -->
                                    <tr>
                                        <td class="td">
                                            <div>
                                                <span> 
                                                    Estimado(a): MARCIA JIMENEZ MORENO ROMERO
                                                </span>
                                                <br/>
                                            <div>
                                            <div>
                                                <p>
                                                    Agradecemos su compra hecha el día 02 de agosto de 2021, por un importe de $1561.20. Le fueron surtidos
                                                    117.60 litros de gas tratado con nuestro exclusivo aditivo, el cual mejora sustancialmente la combustión del
                                                    gas brindandole considerables ahorros para beneficio de su economía.
                                                </p>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="tr-2">
                                        <td class="td3 tanque">
                                            <img src="{{url($msg['tanque'])}}" alt="">
                                        </td>
                                        <td class="td3">
                                            <canvas id="grafica1" width="266" height="180"></canvas>
                                        </td>
                                        <td class="td3">
                                            <canvas id="grafica2" width="266" height="180"></canvas>
                                        </td>
                                    </tr>

                                    <tr class="tr-2">
                                        <td class="td2 p">
                                            <p>
                                                De acuerdo a nuestro sistema de pagos, 
                                                esta compra será cargada a su TARJETA DE 
                                                CREDITO SANTANDER MEXICANO en 2 pagos, en
                                                las siguientes fechas:
                                            </p>                                            
                                        </td>
                                        <td class="td2">
                                            <!-- <canvas id="grafica3" width="350" height="180"></canvas> -->
                                            <canvas id="myChart" width="400" height="200"></canvas>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="height: 30px; width: 100%;">
                                            </div>
                                        </td>                                        
                                    </tr>

                                    <tr class="tr-2">
                                        <td class="td2">
                                            <table class="table-cargos">
                                                <thead>
                                                    <tr>
                                                        <th>Cargo</th>
                                                        <th>Fecha</th>
                                                        <th>Importe</th>
                                                        <th>Uso sistema</th>
                                                        <th>Total</th>
                                                    </tr>                                                    
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>29-10-2021</td>
                                                        <td>50.25</td>
                                                        <td>11.54</td>
                                                        <td>61.79</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>29-10-2021</td>
                                                        <td>50.25</td>
                                                        <td>11.54</td>
                                                        <td>61.79</td>
                                                    </tr>
                                                </tbody>
                                            </table>  
                                            <p style="text-align: center; padding: 0; line-height: 1rem;">SUMA PUNTOS</p> 
                                            <p style="text-align: center; padding: 0; line-height: 1rem;">ESTADO DE CUENTA</p>   
                                            <table class="table-other">
                                                <tbody>
                                                    <tr>
                                                        <td>Anteriores:</td>
                                                        <td>61.79</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Se acumularan:</td>
                                                        <td>61.79</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Saldo nuevo:</td>
                                                        <td>61.79</td>
                                                    </tr>
                                                </tbody>
                                            </table>                                    
                                        </td>
                                        <td class="td2">
                                            <div class="card">
                                                <h4 class="h4">Puntos acumulados</h4>
                                                <p>
                                                    Por esta compra usted acumulará 0 puntos más,
                                                    una vez que hayan sido realizados todos los
                                                    pagos oportunamente. Recuerde que al acumular
                                                    50 puntos los podrá canjear por 50 litros de 
                                                    ¡Gas Gratis!
                                                </p>
                                                <br>
                                                <img src="{{url($msg['cochito'])}}" alt="" width="200px">
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- Agradecimiento -->
                                    <!-- <tr>
                                        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; padding: 0; padding-top: 2em; text-decoration: none;">
                                            <span style="color: #FE7D03; font-size: 30px; font-weight:200;  font-family: Uniform-Medium,  sans-serif">{{__('paid.lbl_agradecimiento_1')}}<br>
                                            <span style="color: #135eac;  font-size: 45px; font-weight:200; font-family: Uniform-Medium,  sans-serif">{{__('paid.lbl_agradecimiento_2')}}<br>
                                        </td>
                                    </tr> -->

                                    <!-- Pregunta-->
                                    <!-- <tr>
                                        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; padding: 0; padding-top: 2em;">
                                            <strong><b style="color: #FE7D03; font-size: 20px; font-weight: 500;">{{__('paid.lbl_ayuda')}}</b></strong><br><br>
                                        </td>
                                    </tr> -->

                                    <tr>
                                        <td class="td">
                                            <div>
                                                <p class="msj-inicio">                                                                                                        
                                                    De acuerdo a su consumo promedio diario de Gas, le surtiremos nuevamente el Jueves 25 de Marzo de 2021.
                                                    Para saber más acerca de nuestro sistema SUMA PUNTOS, consulte nuestro siti web o síganos en Facebook. <br>
                                                    Muchas gracias por su preferencia.
                                                </p>
                                            </div>
                                        </td>
                                    </tr>


                                     <!-- Redes sociales -->
                                     <tr class="tr-2">
                                        <td align="center" class="td1">
                                            <a class="socialLinks" href="{{$msg['urlFb']}}" target="_blank">
                                                <img class="socialIcon" src="{{url($msg['imgFb'])}}" width="60" />
                                            </a>
                                            <a class="socialLinks" href="{{$msg['urlIg']}}" target="_blank">
                                                <img class="socialIcon" src="{{url($msg['imgIg'])}}" width="60"/>
                                            </a>
                                            <a class="socialLinks" href="{{$msg['urlWeb']}}" target="_blank"> 
                                                <img class="socialIcon" src="{{url($msg['imgWeb'])}}" width="60"/>
                                            </a>
                                            <a class="socialLinks" href="{{$msg['urlWs']}}" target="_blank"> 
                                                <img class="socialIcon" src="{{url($msg['imgWs'])}}" width="60" />
                                            </a>
                                            <!-- <div>
                                                
                                            </div>
                                            <div  style="float:left; ">
                                               
                                            </div>
                                            <div style="float:left; ">
                                                
                                            </div>
                                            <div style="float: left">
                                                
                                            </div> -->
                                        </td>
                                    </tr>


                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js" integrity="sha512-Wt1bJGtlnMtGP0dqNFH1xlkLBNpEodaiQ8ZN5JLA5wpc1sUlk/O5uuOMNgvzddzkpvZ9GLyYNa8w2s7rqiTk5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">

        //const grafica1_length = 266;
        //const grafica1_height = 180;        

        var ctx1 = document.getElementById("grafica1");
        var grafica1 = new Chart(ctx1, {
            type: 'bar',
            data: {
                labels: ['Enero','Febrero','Marzo','Abril','Mayo'],
                datasets: [{
                    label: 'Litros surtidos',
                    data: [155, 161, 135, 28, 180],
                    backgroundColor: '#FF941B',
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });



        var ctx2 = document.getElementById("grafica2");
        var grafica2 = new Chart(ctx2, {
            type: 'bar',
            data: {
                labels: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes'],
                datasets: [{
                    label: 'Consumo promedio diario',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: '#004EFD',
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        
        var ctx = document.getElementById('myChart');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio'],
                datasets: [{
                    label: 'Historial de existencia de gas',
                    data: [85, 30, 90, 20, 70, 10],
                    backgroundColor: '#FF941B',
                    borderColor: '#FF941B', 
                    borderWidth: 1,
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });


    </script>
    
</html>
