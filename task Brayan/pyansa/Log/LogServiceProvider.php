<?php

namespace Pyansa\Log;

use Illuminate\Support\ServiceProvider;
use Cake\Core\Configure;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Registra los providers requeridos para el funcionamiento de este
     *
     * @return void
     */
    protected function registerRequiredProviders()
    {
        $this->app->register('Pyansa\Foundation\Providers\ConfigServiceProvider');
    }

    /**
     * Registra la instancia del logger en la aplicacion
     *
     * @return void
     */
    protected function registerLogManager()
    {
        $this->app->instance('log', new LogManager(Configure::read('Log')));
        $this->app->bind('Psr\Log\LoggerInterface', function($app) {
            return $app['log'];
        });
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerRequiredProviders();
        $this->registerLogManager();
    }
}
