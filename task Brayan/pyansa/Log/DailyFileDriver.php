<?php

namespace Pyansa\Log;

use Monolog\Logger as Monolog;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\LineFormatter;

class DailyFileDriver extends Monolog
{
    /**
     * Constructor de la clase
     *
     * @param string $channel
     * @param string $path
     * @param integer $days
     */
    public function __construct($channel, $path, $days = 7)
    {
        $format = "[%datetime%] Channel: %channel% - Level: %level_name%\n%message%\n";
        $handler = new RotatingFileHandler($path, $days);
        $handler->setFormatter(new LineFormatter($format, "Y-m-d H:i:s", true, true));
        parent::__construct($channel, [$handler]);
    }
}
