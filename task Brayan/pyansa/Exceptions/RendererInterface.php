<?php

namespace Pyansa\Exceptions;

interface RendererInterface
{
    /**
     * Renderiza la excepcion
     *
     * @return mixed
     */
    public function render();
}
