<?php

namespace Pyansa\Exceptions;

use Pyansa\Log\ConsoleReport;

class ConsoleRenderer implements RendererInterface
{
    /**
     * Excepcion a renderizar
     *
     * @var Exception
     */
    protected $exception;

    /**
     * Report
     *
     * @var Pyansa\Log\ConsoleReport
     */
    protected $report;

    /**
     * Constructor de la clase
     *
     * @param Exception $exception
     */
    public function __construct($exception, ConsoleReport $report)
    {
        $this->exception = $exception;
        $this->report = $report;
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\RenderInterface::render
     * Renderiza la excepcion
     *
     * @return string
     */
    public function render()
    {
        $exception = $this->exception;
        $report = $this->report;
        $message = sprintf(
            "<error>[LOG_ID: %s] %s</error> %s in [%s, line %s]",
            $report->id,
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );

        return $message;
    }
}
