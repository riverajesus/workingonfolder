<?php

namespace Pyansa\Exceptions;

use ErrorException;

class FatalErrorException extends ErrorException
{
}
