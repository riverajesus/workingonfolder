<?php

namespace Pyansa\Support\Math;

use InvalidArgumentException;

class Math
{
    /**
     * Parte un numero en signo, parte entera y parte decimal
     *
     * @param float $value
     * @return array
     * @throws InvalidArgumentException En caso que el numero sea notacion cientifica
     */
    protected static function splitNumberParts($value)
    {
        if (static::isENotation($value)) {
            throw new InvalidArgumentException("El valor proporcionado es convertido a notacion cientifica: " . $value);
        }

        preg_match("/^(-?)(\d+)\.?(\d*)$/", (string)$value, $matches);

        return array_slice($matches, 1);
    }

    /**
         * Ajusta un numero a `n` decimales. Usando como opciones de ajuste los siguientes tipos:
         *
         * **********************************   "round"    **********************************
         *          -2    -1.5     -1.3     -1     0     1     1.3     1.5     2
         *           ^      |        |       ^           ^       |      |      ^
         *           +------+        +-------+           +-------+      +------+
         *
         * **********************************   "floor"    **********************************
         *          -2    -1.5     -1.3     -1     0     1     1.3     1.5     2
         *           ^      |        |                   ^      |       |
         *           +------+        |                   +------+       |
         *           +---------------+                   +--------------+
         *
         * **********************************   "trunc"    **********************************
         *          -2    -1.5     -1.3     -1     0     1     1.3     1.5     2
         *                  |        |       ^           ^      |       |
         *                  |        +-------+           +------+       |
         *                  +----------------+           +--------------+
         *
         * **********************************   "ceil"    ***********************************
         *          -2    -1.5     -1.3     -1     0     1     1.3     1.5     2
         *                  |        |       ^                  |       |      ^
         *                  |        +-------+                  |       +------+
         *                  +----------------+                  +--------------+
         *
         * NOTA: Las funciones "floor" y "ceil" solo sirven con enteros
         *
         * NOTA: La funcion "round" con su mode PHP_ROUND_HALF_DOWN no se puede usar como truncado
         * ya que si el ultimo digito es >= 6 aun asi redondeara hacia arriba.
         *
         * @param float $value
         * @param integer $n
         * @param string $type
         * @return float
         * @throws InvalidArgumentException En caso que numero de decimales sea mayor a 12
         * @throws InvalidArgumentException En caso que el tipo de ajuste sea invalido
         */
    protected static function decimalAdjust($value, $n = 0, $type = "round")
    {
        $v = $value;
        if ($n > 12) {
            throw new InvalidArgumentException("El ajuste solo funciona hasta 12 decimales");
        }

        if (!static::isENotation($value)) {
            $parts = static::splitNumberParts($value);
            $sign = $parts[0];
            $integer = $parts[1];
            $decimal = str_pad($parts[2], $n, '0');
            $origIntegerLen = strlen($integer);

            // Avanza el punto decimal a la derecha
            preg_match("/(\d{0," . $n ."})(\d*)/", $decimal, $matches);
            $value = floatval($sign . $integer . implode(".", array_slice($matches, 1)));

            // Realiza el ajuste deseado y elimina los decimales sobrantes
            if ($type == "round") {
                $value = round($value);
            } else if ($type == "floor") {
                $value = floor($value);
            } else if ($type == "ceil") {
                $value = ceil($value);
            } else if ($type == "trunc") {
                $value = $value < 0 ? ceil($value) : floor($value);
            } else {
                throw new InvalidArgumentException("Tipo de ajuste inválido: " . $type);
            }

            // Avanza el punto decimal a la izquierda
            $parts = static::splitNumberParts($value);
            $sign = $parts[0];
            $integer = str_pad($parts[1], $origIntegerLen + $n, '0', STR_PAD_LEFT);
            preg_match("/(\d*)(\d{" . $n . "})/", $integer, $matches);
            $value = floatval($sign . implode(".", array_slice($matches, 1)));

            return $value;
        } else {
            $parts = explode("E", (string)$value);
            $base = $parts[0];
            $exponent = isset($parts[1]) ? intval($parts[1]) : 0;
            $value = floatval($base . 'E' . ($exponent + $n));

            // Realiza el ajuste deseado y elimina los decimales sobrantes
            if ($type == "round") {
                $value = round($value);
            } else if ($type == "floor") {
                $value = floor($value);
            } else if ($type == "ceil") {
                $value = ceil($value);
            } else if ($type == "trunc") {
                $value = $value < 0 ? ceil($value) : floor($value);
            } else {
                throw new InvalidArgumentException("Tipo de ajuste inválido: " . $type);
            }

            $parts = explode("E", (string)$value);
            $base = $parts[0];
            $exponent = isset($parts[1]) ? intval($parts[1]) : 0;
            $value = floatval($base . 'E' . ($exponent - $n));

            return $value;
        }
    }

    /**
     * Compara dos numeros tomando en cuenta la precision
     *
     * @param float $a
     * @param float $b
     * @param integer $scale
     * @return boolean
     */
    public static function equal($a, $b, $scale = 6)
    {
        return function_exists("bccomp") ? bccomp($a, $b, $scale) === 0 : BCMath::comp($a, $b, $scale) === 0;
    }

    /**
     * Retorna true si $value se convierte en notacion cientifica al parsearlo a string
     *
     * @param float $value
     * @return boolean
     */
    public static function isENotation($value)
    {
        $value = (string)$value;

        return (bool)preg_match("/e/i", $value);
    }

    /**
     * Obtiene la cantidad de decimales de $value
     *
     * @param float $value
     * @return integer
     */
    public static function countDecimals($value)
    {
        $parts = static::splitNumberParts($value);

        return strlen($parts[2]);
    }

    /**
     * Trunca $value a la cantidad de decimales dada por $decimals
     *
     * @param float $value
     * @param integer $decimals
     * @return float
     */
    public static function trunc($value, $decimals)
    {
        return static::decimalAdjust($value, $decimals, "trunc");
    }

    /**
     * Redondea $value a la cantidad de decimales dada por $decimals
     *
     * @param float $value
     * @param integer $decimals
     * @return float
     */
    public static function round($value, $decimals)
    {
        return static::decimalAdjust($value, $decimals, "round");
    }

    /**
     * Ceil $value a la cantidad de decimales dada por $decimals
     *
     * @param float $value
     * @param integer $decimals
     * @return float
     */
    public static function ceil($value, $decimals)
    {
        return static::decimalAdjust($value, $decimals, "ceil");
    }

    /**
     * Floor $value a la cantidad de decimales dada por $decimals
     *
     * @param float $value
     * @param integer $decimals
     * @return float
     */
    public static function floor($value, $decimals)
    {
        return static::decimalAdjust($value, $decimals, "floor");
    }
}
