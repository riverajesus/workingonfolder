<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/test/{id}', 'CorreoInformacionController@conectar');

$router->get('/test2/{id}', 'CorreoInformacionController@index');

$router->get('/pago/{id}', 'PagoRealizadoController@conectar');
$router->get('/pago2/{id}', 'PagoRealizadoController@show');

$router->get('actualizacion', 'CorreoInformacionController@show');

# Rutas para clientes
$router->get('/clientesIndex/{id}', 'CorreoClientesController@index');
$router->get('/clientesConectar/{id}', 'CorreoClientesController@conectar');

$router->get('detect-device', function () {
    $agent = new  \Jenssegers\Agent\Agent;

    $result1 = $agent->isDesktop();
    $result2 = $agent->isMobile();
    $result3 = $agent->isTablet();

    echo $result1." , ".$result2." , ".$result3;
});
