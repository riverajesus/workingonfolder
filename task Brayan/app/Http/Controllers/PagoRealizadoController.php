<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\CorreoPago;

class PagoRealizadoController extends Controller
{
    public function show($id)
    {
        $res = app('db')->select("SELECT * FROM datos WHERE plaza_id = $id");


        if($res){
            $driv='mysql';
            $puerto_conex= 3306;
            $database_name= $res[0]->db_name;
            $user_name= $res[0]->user_name;
            $contra= $res[0]->pass;
            $host_con= $res[0]->host_name;
            
            Config::set([
                'database.connections.server_variable.driver'=>$driv,
                'database.connections.server_variable.database'=>$database_name,   
                'database.connections.server_variable.username'=>$user_name,
                'database.connections.server_variable.password'=>$contra,
                'database.connections.server_variable.port'=>$puerto_conex,
                'database.connections.server_variable.host'=>$host_con,
            ]);
            $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM recur");
            
            Config::set([
                'database.connections.server_variable' => null,
            ]);
            

            
            $mensaje = array(
                'lineas' => $respuesta[11]->ruta,
                'tanque' => $respuesta[1]->ruta,
                'cochito' => $respuesta[2]->ruta,
                'urlFb' => $respuesta[3]->ruta,
                'imgFb' => $respuesta[4]->ruta,
                'urlIg' => $respuesta[5]->ruta,
                'imgIg' => $respuesta[6]->ruta,
                'urlWeb' => $respuesta[7]->ruta,
                'imgWeb' => $respuesta[8]->ruta,
                'urlWs' => $respuesta[9]->ruta,
                'imgWs' => $respuesta[10]->ruta,
                'id' => $id
            );
            
            return view('llenafacil.pago-realizado1', ["msg" => $mensaje]);
             
        }else{
            return array('error' => 0, 'response'=> 'Id not found in db, please try another');
        }
    }
    public function conectar($id){

        $res = app('db')->select("SELECT * FROM datos WHERE plaza_id = $id");


        if($res){
            $driv='mysql';
            $puerto_conex= 3306;
            $database_name= $res[0]->db_name;
            $user_name= $res[0]->user_name;
            $contra= $res[0]->pass;
            $host_con= $res[0]->host_name;
            
            Config::set([
                'database.connections.server_variable.driver'=>$driv,
                'database.connections.server_variable.database'=>$database_name,   
                'database.connections.server_variable.username'=>$user_name,
                'database.connections.server_variable.password'=>$contra,
                'database.connections.server_variable.port'=>$puerto_conex,
                'database.connections.server_variable.host'=>$host_con,
            ]);
            $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM recur");
            
            Config::set([
                'database.connections.server_variable' => null,
            ]);
            

            
            $mensaje = array(
                'lineas' => $respuesta[0]->ruta,
                'tanque' => $respuesta[1]->ruta,
                'cochito' => $respuesta[2]->ruta,
                'urlFb' => $respuesta[3]->ruta,
                'imgFb' => $respuesta[4]->ruta,
                'urlIg' => $respuesta[5]->ruta,
                'imgIg' => $respuesta[6]->ruta,
                'urlWeb' => $respuesta[7]->ruta,
                'imgWeb' => $respuesta[8]->ruta,
                'urlWs' => $respuesta[9]->ruta,
                'imgWs' => $respuesta[10]->ruta,
                'id' => $id
            );
            //return $mensaje;
            $asunto = "Pago realizado";
            //Mail::to('2016030154@upsin.edu.mx')->send(new CorreoPago($mensaje,$asunto));
            //Mail::to('jesusguerrero97@hotmail.com')->send(new CorreoPago($mensaje,$asunto));
            //Mail::to('grivera@sersi.com.mx')->send(new CorreoPago($mensaje,$asunto));
            //return array('error' => 0, 'response'=> 'Correo enviado con éxito!');
            return view('llenafacil.pago-realizado', ["msg" => $mensaje]);
             
        }else{
            return array('error' => 0, 'response'=> 'Id not found in db, please try another');
        }

       
    }
}
