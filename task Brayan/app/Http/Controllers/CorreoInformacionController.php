<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\CorreoEnviado;

class CorreoInformacionController extends Controller
{
    public function show(){
        $agent = new  \Jenssegers\Agent\Agent;

        $result1 = $agent->isDesktop();
        $result2 = $agent->isMobile();
        $result3 = $agent->isTablet();

        echo $result1." , ".$result2." , ".$result3;
    }

    public function conectar($id)
    {
        $res = app('db')->select("SELECT * FROM datos WHERE plaza_id = $id");


        if($res){
            $driv='mysql';
            $puerto_conex= 3306;
            $database_name= $res[0]->db_name;
            $user_name= $res[0]->user_name;
            $contra= $res[0]->pass;
            $host_con= $res[0]->host_name;
            
            Config::set([
                'database.connections.server_variable.driver'=>$driv,
                'database.connections.server_variable.database'=>$database_name,   
                'database.connections.server_variable.username'=>$user_name,
                'database.connections.server_variable.password'=>$contra,
                'database.connections.server_variable.port'=>$puerto_conex,
                'database.connections.server_variable.host'=>$host_con,
            ]);
            $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM recursos");
           
            Config::set([
                'database.connections.server_variable' => null,
            ]);
            
            $mensaje = array(
                'icono' => $respuesta[0]->ruta,
                'clase' => $respuesta[1]->ruta,
                'urlFb' => $respuesta[2]->ruta,
                'imgFb' => $respuesta[3]->ruta,
                'urlIg' => ($respuesta[4]->ruta != '') ? $respuesta[4]->ruta : "" ,
                'imgIg' => $respuesta[5]->ruta,
                'urlWeb' => $respuesta[6]->ruta,
                'imgWeb' => $respuesta[7]->ruta,
                'urlWs' => $respuesta[8]->ruta,
                'imgWs' => $respuesta[9]->ruta,
                'logoGral' => $respuesta[10]->ruta,
                'playstore' => $respuesta[11]->ruta,
                'appstore' => $respuesta[12]->ruta,
                'appgallery' => $respuesta[13]->ruta,
                'url-appgallery' => $respuesta[14]->ruta,
                'url-appstore' => $respuesta[15]->ruta,
                'id' => $id
            );

            $url = "http://migaspasa.gaspasa.com.mx/demo/Plazas/getCorreosPrueba";
            // Los datos JSON
            $datos = [
                "key" => "ef73781effc5774100f87fe2f437a435",
                "app" => "2",
            ];
           // Crear opciones de la petición HTTP
            $opciones = array(
                "http" => array(
                    "header" => "Content-type: application/x-www-form-urlencoded\r\n",
                    "method" => "POST",
                    "content" => http_build_query($datos), # Agregar el contenido definido antes
                ),
            );
            # Preparar petición
            $contexto = stream_context_create($opciones);
            # Hacerla
            $resultado = file_get_contents($url, false, $contexto);
            if ($resultado === false) {
                echo "Error haciendo petición";
                exit;
            }

            # si no salimos allá arriba, todo va bien
            //var_dump($resultado);

            $array = json_decode($resultado,true);
            //var_dump($array);
            
            // foreach($array as $value)
            // {
            //     print_r($value);
            //     array_push($hola,$value);
            // }

            //return $array['correos'];

            $prueba = explode(',',$array['correos']);

            $replace = str_replace("'","",$prueba);

            $correos = [];
            foreach($replace as $value)
            {
               array_push($correos,trim($value));
            }
            // return $correos;
            
            //return $mensaje;
            $asunto = "Siguenos desde la App";
            Mail::to("jesusguerrero97@hotmail.com")->send(new CorreoEnviado($mensaje,$asunto));

            

            return array('error' => 0, 'response'=> 'Correo enviado con éxito! 1');
            //return view('actualizacion.correo_informacion', ["respuesta" => $respuesta]);
            //return $mensaje; 
        }else{
            return array('error' => 0, 'response'=> 'Id not found in db, please try another');
        }
    }
  
    public function index($id)
    {
        /*$contador = 0;
        for($x=0;$x<=10;$x++)
        {
            echo $x;
            if($contador == 5 && $x < 10)
            {
                sleep(10);
                $contador = 0;
                echo "mitad";
            }
            $contador++;
        }

        
        return;*/
        $res = app('db')->select("SELECT * FROM datos WHERE plaza_id = $id");


        if($res){
            $driv='mysql';
            $puerto_conex= 3306;
            $database_name= $res[0]->db_name;
            $user_name= $res[0]->user_name;
            $contra= $res[0]->pass;
            $host_con= $res[0]->host_name;
            
            Config::set([
                'database.connections.server_variable.driver'=>$driv,
                'database.connections.server_variable.database'=>$database_name,   
                'database.connections.server_variable.username'=>$user_name,
                'database.connections.server_variable.password'=>$contra,
                'database.connections.server_variable.port'=>$puerto_conex,
                'database.connections.server_variable.host'=>$host_con,
            ]);
            $respuesta = app('db')->connection('server_variable')->select("SELECT * FROM recursos");
           
            Config::set([
                'database.connections.server_variable' => null,
            ]);
            //return $respuesta;
            $mensaje = array(
                'icono' => $respuesta[0]->ruta,
                'clase' => $respuesta[1]->ruta,
                'urlFb' => $respuesta[2]->ruta,
                'imgFb' => $respuesta[3]->ruta,
                'urlIg' => $respuesta[4]->ruta,
                'imgIg' => $respuesta[5]->ruta,
                'urlWeb' => $respuesta[6]->ruta,
                'imgWeb' => $respuesta[7]->ruta,
                'urlWs' => $respuesta[8]->ruta,
                'imgWs' => $respuesta[9]->ruta,
                'logoGral' => $respuesta[10]->ruta,
                'playstore' => $respuesta[11]->ruta,
                'appstore' => $respuesta[12]->ruta,
                'appgallery' => $respuesta[13]->ruta,
                'url-appgallery' => $respuesta[14]->ruta,
                'url-appstore' => $respuesta[15]->ruta,
                'id' => $id
            );
            
            //return $mensaje;

            //return array('error' => 0, 'response'=> 'Correo enviado con éxito!');
            return view('actualizacion.correo_informacion', ["msg" => $mensaje]);
            //return $mensaje; 
        }else{
            return array('error' => 0, 'response'=> 'Id not found in db, please try another');
        }

    }


}