<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use App\Mail\CorreoClientes;

class CorreoClientesController extends Controller
{
    public function show(){
        $agent = new  \Jenssegers\Agent\Agent;

        $result1 = $agent->isDesktop();
        $result2 = $agent->isMobile();
        $result3 = $agent->isTablet();

        echo $result1." , ".$result2." , ".$result3;
    }

    public function conectar($id)
    {
        set_time_limit(0);


        if($id == 1){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/gaspasa/GASPASA_MAIL.jpg",
                'id' => $id
            );
            $res = app('db')->select("SELECT * FROM gaspasa WHERE enviado = 0");
        }
        if($id == 2){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/diesgas/DIESGAS_MAIL.jpg",
                'id' => $id
            );
            $res = app('db')->select("SELECT * FROM diesgas WHERE enviado = 0");
        }
        if($id == 3){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/caligas/CALIGAS_MAIL.jpg",
                'id' => $id
            );
            $res = app('db')->select("SELECT * FROM caligas WHERE enviado = 0;");
        }

        $count = count($res);
        $asunto = "¡Felices Fiestas!";
        $contador = 1;
        for($x=0;$x<$count;$x++)
        {
            //Mailecho $x;
            try {
                 Mail::to(trim($res[$x]->correo))->send(new CorreoClientes($mensaje,$asunto));
                $respuesta = app('db')->select("UPDATE diesgas SET enviado = 1 WHERE correo ='".$res[$x]->correo."';");
            } catch (\Exception $e) {
                
            }
            
            if($contador == 900 && $x < $count)
            {
                //sleep(3000);
                $contador = 0;
                echo "lote hasta ".$x;
            }
            $contador++;
        }

        return array('error' => 0, 'response'=> 'Correos enviado con éxito!');
        /*for($x=0;$x<$count;$x++)
        {   
            $respuesta = app('db')->select("UPDATE gaspasa SET enviado = 1 WHERE correo ='".$res[$x]->correo."';");
            
            print_r($respuesta);
            echo $x;
            //echo trim($res[$x]->correo);
            //var_dump($res[$x]->correo);
            //Mail::to(trim($res[$x]->correo))->send(new CorreoEnviado($mensaje,$asunto));
        }*/
        
    }
  
    public function index($id)
    {
        if($id == 1){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/gaspasa/_MAIL.jpg",
                'id' => $id
            );
        }
        if($id == 2){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/diesgas/_MAIL.jpg",
                'id' => $id
            );
        }
        if($id == 3){
            $mensaje = array(
                'imagen' => "https://www.sersi.com.mx:8084/img/caligas/_MAIL.jpg",
                'id' => $id
            );
        }
        
        
        //return $mensaje;

        //return array('error' => 0, 'response'=> 'Correo enviado con éxito!');
        return view('clientes.clientes', ["msg" => $mensaje]);
        //return $mensaje; 

    }


}