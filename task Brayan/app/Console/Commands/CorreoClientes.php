<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\CorreoClientes as CorreoClientesMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Exception;
use Pyansa\Log\ConsoleReport;

class CorreoClientes extends Command
{
    /**
     * Nombre del comando
     *
     * @var string
     */
    protected $signature = "CorreoClientes";

    /**
     * Correos que se enviaran cada ves que se ejecute el comando
     *
     * @var integer
     */
    const CORREOS_POR_PAGINA = 50;

    /**
     * Conexion a la base de datos
     *
     * @var Illuminate\Database\Connection
     */
    public $db;

    /**
     * Marcas de empresas
     *
     * @var array
     */
    public $marcas = [
        1 => "gaspasa",
        2 => "diesgas",
        3 => "caligas",
    ];

    /**
     * Inicio del comando
     *
     * @return void
     */
    public function handle()
    {
        $this->db = DB::connection('mysql');

        // Inicia la transaccion
        $this->db->beginTransaction();
        $pagina = $this->getSiguientePagina();

        // Si ya no hay paginas, se termina el proceso
        if ($pagina === null) {
            return;
        }

        switch ($pagina['marca']) {
            case 1:
                $imagen = "https://www.sersi.com.mx:8084/img/gaspasa/_MAIL.jpg";
                break;
            case 2:
                $imagen = "https://www.sersi.com.mx:8084/img/diesgas/_MAIL.jpg";
                break;
            case 3:
                $imagen = "https://www.sersi.com.mx:8084/img/caligas/_MAIL.jpg";
                break;
            default:
                throw new Exception("El id de la empresa no es valido");
                break;
        }

        $mensaje = [
            "imagen" => $imagen,
            "id" => $pagina["marca"]
        ];
        $asunto = "¡Felices Fiestas!";

        foreach ($pagina["registros"] as &$row) {
            $row->correo = trim($row->correo);
            // DEBUG
            // $row->correo = "b.venegas@sersi.com.mx";
            $validator = Validator::make(
                [
                    "correo" => $row->correo
                ],
                [
                    "correo" => "email"
                ]
            );

            // Si el correo es invalido se salta a la siguiente iteracion
            if ($validator->fails()) {
                $this->error(sprintf("[%s] Correo invalido: %s", date("Y-m-d H:i:s"), $row->correo));
                continue;
            }

            // TODO
            // Envia el correo
            try {
                Mail::to($row->correo)->bcc(["grivera@sersi.com.mx","b.venegas@sersi.com.mx"])->send(new CorreoClientesMail($mensaje, $asunto));
                $this->info(sprintf("[%s] Correo enviado: %s", date("Y-m-d H:i:s"), $row->correo));
            } catch(Exception $ex) {
                // Se usa este try/catch para saltar los errores de envio de
                $this->error(sprintf("[%s] Error al enviar correo: %s", date("Y-m-d H:i:s"), $row->correo));
            }
        }
        unset($row);

        // Marca los registros como enviado
        $this->marcarPagina($pagina);
        $this->db->commit();
    }

    /**
     * Marca los registros de una pagina como enviados
     *
     * @param array $pagina
     * @return void
     */
    public function marcarPagina($pagina)
    {
        $tabla = $this->marcas[$pagina["marca"]];
        $query = "UPDATE {$tabla} SET " .
                "enviado = 1 " .
                "WHERE id IN ({ids})";
        $query = str_replace(
            "{ids}",
            implode(", ", str_split(str_repeat("?", count($pagina["registros"])))),
            $query
        );
        $queryParams = Arr::pluck($pagina["registros"], "id");
        $result = $this->db->update($query, $queryParams);
    }

    /**
     * Obtiene una pagina de registros de la empresa que corresponda segun los correos que falten por enviarse
     *
     * @return array
     */
    public function getSiguientePagina()
    {
        foreach ($this->marcas as $id => $tabla) {
            $query = "SELECT COUNT(*) AS cantidad " .
                "FROM {$tabla} " .
                "WHERE enviado = ?";
            $queryParams = [0];
            $result = $this->db->select($query, $queryParams);
            $cantidad = reset($result)->cantidad;

            // Si ya no hay correos por enviar de esa marca se salta a la siguiente
            if ($cantidad === 0) {
                continue;
            }

            $query = "SELECT * " .
                "FROM {$tabla} " .
                "WHERE enviado = ? " .
                "LIMIT ? " .
                "FOR UPDATE";
            $queryParams = [0, self::CORREOS_POR_PAGINA];
            $result = $this->db->select($query, $queryParams);

            return [
                "marca" => $id,
                "registros" => $result
            ];
        }

        return null;
    }
}