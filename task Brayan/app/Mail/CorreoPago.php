<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CorreoPago extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "Pago realizado";
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensaje, $subject)
    {          
        $this->msg = $mensaje;
        $this->subject = $subject;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('llenafacil.pago-realizado'); 
    }
}