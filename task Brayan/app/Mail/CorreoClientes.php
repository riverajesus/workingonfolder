<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CorreoClientes extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "¡Felices fiestas!";
    public $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensaje, $subject)
    {          
        $this->msg = $mensaje;
        $this->subject = $subject;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        switch ($this->msg['id']) {
            case 1:
                return $this->from('ayuda@gaspasa.com.mx','GASPASA')->view('clientes.clientes');
                break;
            case 2:
                return $this->from('ayuda@diesgas.com.mx','DIESGAS')->view('clientes.clientes');
                break;
            case 3:
                return $this->from('ayuda@caligas.com.mx','CALIGAS')->view('clientes.clientes');
                break;
        }
         
    }
}